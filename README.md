# Interactive path tracer written on rust with wgpu-hal
This is intended to be some kind of 3d engine with path tracing as the main rendering method. The goal is to push and popularize path traced rendering and replace rasterized rendering.

It is based on [wgpu tutorial](https://sotrh.github.io/learn-wgpu/intermediate/tutorial10-lighting) but rewritten using [wgpu-hal](https://crates.io/crates/wgpu-hal) instead of wgpu to make use of hardware-accelerated ray tracing.
You need a GPU with support for raytracing extensions (GTX 1060 and newer/RX 6500 and newer). [Egui](https://crates.io/crates/egui) is used for UI.
* Click an object to select (see `Selected object` in UI, no outline yet)
* Edit translation and rotation
* `Parameter 1` controls mix between current and previous frame (denoiser at home)