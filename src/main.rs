#![allow(non_snake_case)]

use std::iter::once;
use std::mem::size_of;
use std::mem::size_of_val;
use std::num::NonZeroU64;
use std::ptr::copy_nonoverlapping;

use futures::executor::block_on;
use glam::{Mat4, Quat, Vec3A};
use wgpu_hal::{CommandEncoder, Device, Queue};
use wgpu_types as wgt;
use wgpu_types::Origin2d;
use winit::event::{ElementState, MouseButton};

use app::App;

use crate::accel_struct::AccelerationStructureInstance;
use crate::texture::Texture;

mod accel_struct;
mod app;
mod camera;
mod filtering;
mod g_buffers;
mod gui;
mod model;
mod post_processing;
mod resources;
mod texture;

fn write_buffer(content: &[u8], device: &wgpu_hal::vulkan::Device, buffer: &wgpu_hal::vulkan::Buffer) {
    unsafe {
        let size = size_of_val(content) as u64;
        let mapping = device.map_buffer(buffer, 0..size).unwrap();
        assert!(mapping.is_coherent);
        copy_nonoverlapping(content.as_ptr(), mapping.ptr.as_ptr(), size as usize);
        device.unmap_buffer(buffer);
    }
}

///Use when not recording
fn write_texture(
    content: &[u8],
    device: &wgpu_hal::vulkan::Device,
    texture: &Texture,
    origin2d: Origin2d,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    queue: &mut wgpu_hal::vulkan::Queue,
) {
    unsafe {
        let size = size_of_val(content);
        let mapping = device.map_buffer(&texture.texture_buffer, 0..size as u64).unwrap();
        assert!(mapping.is_coherent);
        copy_nonoverlapping(content.as_ptr(), mapping.ptr.as_ptr(), size);
        device.unmap_buffer(&texture.texture_buffer);
        command_encoder.begin_encoding(None).unwrap();
        command_encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
            buffer: &texture.texture_buffer,
            usage: wgpu_hal::BufferUses::COPY_DST..wgpu_hal::BufferUses::COPY_SRC,
        }));
        command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
            texture: &texture.texture,
            range: Default::default(),
            usage: wgpu_hal::TextureUses::UNINITIALIZED..wgpu_hal::TextureUses::COPY_DST,
        }));
        command_encoder.copy_buffer_to_texture(
            &texture.texture_buffer,
            &texture.texture,
            once(wgpu_hal::BufferTextureCopy {
                buffer_layout: wgpu_types::ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some(4 * texture.size.width),
                    rows_per_image: Some(texture.size.height),
                },
                texture_base: wgpu_hal::TextureCopyBase {
                    mip_level: 0,
                    array_layer: 0,
                    origin: origin2d.to_3d(0),
                    aspect: wgpu_hal::FormatAspects::COLOR,
                },
                size: wgpu_hal::CopyExtent {
                    width: texture.size.width,
                    height: texture.size.height,
                    depth: 1,
                },
            }),
        );
        command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
            texture: &texture.texture,
            range: Default::default(),
            usage: wgpu_hal::TextureUses::COPY_DST..wgpu_hal::TextureUses::RESOURCE,
        }));
        command_encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
            buffer: &texture.texture_buffer,
            usage: wgpu_hal::BufferUses::COPY_SRC..wgpu_hal::BufferUses::COPY_DST,
        }));
        submit_and_wait(command_encoder, device, queue).unwrap();
    }
}
fn submit_and_wait(
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    device: &wgpu_hal::vulkan::Device,
    queue: &wgpu_hal::vulkan::Queue,
) -> Result<bool, wgpu_hal::DeviceError> {
    unsafe {
        let mut init_fence = device.create_fence().unwrap();
        let cmd_buf = command_encoder.end_encoding().unwrap();
        queue.submit(&[&cmd_buf], &[], (&mut init_fence, 1)).unwrap();
        let r = device.wait(&init_fence, 1, !0);
        device.destroy_fence(init_fence);
        command_encoder.reset_all(once(cmd_buf));
        r
    }
}
#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct CameraUniform {
    view_proj: [[[f32; 4]; 4]; 3],
    view_position: [f32; 4],
}
impl CameraUniform {
    fn new() -> Self {
        Self {
            view_position: [0.0; 4],
            view_proj: [Mat4::default().to_cols_array_2d(); 3],
        }
    }
    fn update_view_proj(&mut self, camera: &camera::Camera, projection: &camera::Projection) {
        self.view_proj = [
            (camera::OPENGL_TO_WGPU_MATRIX * projection.calc_matrix() * camera.calc_matrix()).to_cols_array_2d(),
            projection.calc_matrix().to_cols_array_2d(),
            camera.calc_matrix().to_cols_array_2d(),
        ];
        self.view_position = camera.position.extend(1.0).to_array();
    }
    const SIZE: NonZeroU64 = unsafe { NonZeroU64::new_unchecked(size_of::<CameraUniform>() as u64) };
}

struct ObjInstance {
    position: Vec3A,
    rotation: Quat,
    scale: Vec3A,
}
impl ObjInstance {
    fn to_raw(&self) -> InstanceRaw {
        InstanceRaw {
            model: (Mat4::from_translation(self.position.into()) * Mat4::from_quat(self.rotation) * Mat4::from_scale(self.scale.into()))
                .to_cols_array_2d(),
            normal: glam::Mat3A::from_quat(self.rotation).to_cols_array_2d(),
        }
    }
}
#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct InstanceRaw {
    model: [[f32; 4]; 4],
    normal: [[f32; 3]; 3],
}
impl InstanceRaw {
    fn _desc() -> wgpu_hal::VertexBufferLayout<'static> {
        wgpu_hal::VertexBufferLayout {
            array_stride: size_of::<InstanceRaw>() as u64,
            // We need to switch from using a step mode of Vertex to Instance
            // This means that our shaders will only change to use the next
            // instance when the shader starts processing a new instance
            step_mode: wgt::VertexStepMode::Instance,
            attributes: &[
                // A mat4 takes up 4 vertex slots as it is technically 4 vec4s. We need to define a slot
                // for each vec4. We'll have to reassemble the mat4 in the shader.
                wgt::VertexAttribute {
                    offset: 0,
                    // While our vertex shader only uses locations 0, and 1 now, in later tutorials, we'll
                    // be using 2, 3, and 4, for Vertex. We'll start at slot 5, not conflict with them later
                    shader_location: 5,
                    format: wgt::VertexFormat::Float32x4,
                },
                wgt::VertexAttribute {
                    offset: size_of::<[f32; 4]>() as u64,
                    shader_location: 6,
                    format: wgt::VertexFormat::Float32x4,
                },
                wgt::VertexAttribute {
                    offset: size_of::<[f32; 8]>() as u64,
                    shader_location: 7,
                    format: wgt::VertexFormat::Float32x4,
                },
                wgt::VertexAttribute {
                    offset: size_of::<[f32; 12]>() as u64,
                    shader_location: 8,
                    format: wgt::VertexFormat::Float32x4,
                },
                wgt::VertexAttribute {
                    offset: size_of::<[f32; 16]>() as u64,
                    shader_location: 9,
                    format: wgt::VertexFormat::Float32x3,
                },
                wgt::VertexAttribute {
                    offset: size_of::<[f32; 19]>() as u64,
                    shader_location: 10,
                    format: wgt::VertexFormat::Float32x3,
                },
                wgt::VertexAttribute {
                    offset: size_of::<[f32; 22]>() as u64,
                    shader_location: 11,
                    format: wgt::VertexFormat::Float32x3,
                },
            ],
        }
    }
}
struct TlasContainer {
    tlas: wgpu_hal::vulkan::AccelerationStructure,
    instances: Vec<AccelerationStructureInstance>,
    scratch: wgpu_hal::vulkan::Buffer,
    instances_buffer: wgpu_hal::vulkan::Buffer,
}
struct BlasContainer {
    blas: wgpu_hal::vulkan::AccelerationStructure,
    scratch: wgpu_hal::vulkan::Buffer,
}

#[rustfmt::skip]
pub const OPENGL_TO_WGPU_MATRIX: Mat4 = Mat4::from_cols_array(&[
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.5,
    0.0, 0.0, 0.0, 1.0,
]);
const NUM_INSTANCES_PER_ROW: u32 = 10;

fn main() {
    // env_logger::init();
    unsafe {
        let event_loop = winit::event_loop::EventLoopBuilder::new().build().unwrap();
        let mut app = block_on(App::init(&event_loop));
        let mut fence_value = wgpu_hal::FenceValue::from(1u64);
        let mut fence = app.device.create_fence().unwrap();
        let mut last_render_time = std::time::Instant::now();
        app.queue.submit(&[], &[], (&mut fence, fence_value)).unwrap();
        app.device.wait(&fence, fence_value, !0).unwrap();
        fence_value += 1;
        let (camera_buffer_mapping, camera_uniform_size) = (
            app.device
                .map_buffer(&app.camera_buffer, 0..size_of_val(&app.camera_uniform) as _)
                .unwrap(),
            size_of_val(&app.camera_uniform),
        );
        let (param_buffer_mapping, param_size) = (
            app.device
                .map_buffer(&app.custom_buffer, 0..size_of_val(&app.state.custom_parameter) as _)
                .unwrap(),
            size_of_val(&app.state.custom_parameter),
        );
        let (rng_buffer_mapping, rng_buffer_size) = (
            app.device.map_buffer(&app.rng_buffer, 0..size_of::<u32>() as _).unwrap(),
            size_of::<u32>(),
        );
        let _ = event_loop.run(|event, target| {
            if let winit::event::Event::WindowEvent { event: i, .. } = &event {
                let _ = app.egui_state.on_window_event(&app.window, i);
            };
            match event {
                winit::event::Event::AboutToWait => app.window.request_redraw(),
                winit::event::Event::DeviceEvent {
                    event: winit::event::DeviceEvent::MouseMotion { delta },
                    ..
                } => {
                    if app.state.mouse_pressed {
                        app.camera_controller.process_mouse(delta.0, delta.1);
                    }
                }
                winit::event::Event::WindowEvent {
                    event: winit::event::WindowEvent::KeyboardInput { .. },
                    ..
                } => {
                    let winit::event::Event::WindowEvent { event: y, .. } = event else {
                        unreachable!()
                    };
                    app.input(&y);
                }
                winit::event::Event::WindowEvent {
                    event:
                        winit::event::WindowEvent::MouseInput {
                            state,
                            button: MouseButton::Left,
                            ..
                        },
                    ..
                } => {
                    app.state.mouse_pressed = (state == ElementState::Pressed) & !app.state.interacting_with_ui;
                }
                winit::event::Event::WindowEvent {
                    event: winit::event::WindowEvent::CloseRequested,
                    ..
                } => {
                    std::thread::sleep(std::time::Duration::from_nanos(2000));
                    target.exit()
                }
                winit::event::Event::WindowEvent {
                    event: winit::event::WindowEvent::RedrawRequested,
                    ..
                } => {
                    app.update_state();
                    copy_nonoverlapping(
                        bytemuck::cast_slice(&[app.state.custom_parameter]).as_ptr(),
                        param_buffer_mapping.ptr.as_ptr(),
                        param_size,
                    );
                    copy_nonoverlapping(
                        bytemuck::cast_slice(&[app.camera_uniform]).as_ptr(),
                        camera_buffer_mapping.ptr.as_ptr(),
                        camera_uniform_size,
                    );
                    copy_nonoverlapping(
                        bytemuck::cast_slice(&[fastrand::u32(..)]).as_ptr(),
                        rng_buffer_mapping.ptr.as_ptr(),
                        rng_buffer_size,
                    );
                    app.render(&mut fence, fence_value, &mut last_render_time);
                    copy_nonoverlapping(
                        param_buffer_mapping.ptr.as_ptr(),
                        app.state.return_from_shader.as_mut_ptr(),
                        param_size,
                    );
                    let ret_long = *bytemuck::from_bytes::<[f32; 16]>(&app.state.return_from_shader);
                    app.state.custom_parameter = ret_long;
                    if app.state.mouse_pressed {
                        app.state.selected_index = ret_long[3] as u32
                    };
                    fence_value += 1;
                    if let Some(u) = app.state.is_resize_needed {
                        app.resize(u);
                        app.state.is_resize_needed = None;
                    }
                }
                winit::event::Event::WindowEvent {
                    event: winit::event::WindowEvent::Resized(u),
                    ..
                } => {
                    if app.surface_config.extent.height != u.height || app.surface_config.extent.width != u.width {
                        app.state.is_resize_needed = Some(u)
                    }
                }
                _ => {}
            }
        });
        app.device.destroy_fence(fence);
        app.destroy();
    }
}
