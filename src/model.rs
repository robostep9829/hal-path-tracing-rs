use std::iter::once;

use bytemuck::cast_slice;
use glam::{Mat3, Mat4, Vec3};
use rayon::prelude::*;
use wgpu_hal::{CommandEncoder, Device};

use crate::app::joint_matrix;

use super::{resources, texture, write_buffer, BlasContainer};

pub trait Vertex {
    fn desc() -> wgpu_hal::VertexBufferLayout<'static>;
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct ModelVertex {
    pub position: [f32; 3],
    pub tex_coords: [f32; 2],
    pub normal: [f32; 3],
    pub joints: [u16; 4],
    pub weights: [f32; 4],
}

impl Vertex for ModelVertex {
    fn desc() -> wgpu_hal::VertexBufferLayout<'static> {
        use std::mem;
        wgpu_hal::VertexBufferLayout {
            array_stride: mem::size_of::<ModelVertex>() as u64,
            step_mode: wgpu_types::VertexStepMode::Vertex,
            attributes: &[
                wgpu_types::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu_types::VertexFormat::Float32x3,
                },
                wgpu_types::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as u64,
                    shader_location: 1,
                    format: wgpu_types::VertexFormat::Float32x2,
                },
                wgpu_types::VertexAttribute {
                    offset: mem::size_of::<[f32; 5]>() as u64,
                    shader_location: 2,
                    format: wgpu_types::VertexFormat::Float32x3,
                },
            ],
        }
    }
}

pub struct Model {
    pub materials: Vec<Material>,
    pub blas: BlasContainer,
}
pub struct Material {
    pub name: String,
    pub diffuse_texture: texture::Texture,
    pub vertex_buffer: wgpu_hal::vulkan::Buffer,
    pub index_buffer: wgpu_hal::vulkan::Buffer,
    pub vertex_num: u32,
    pub index_num: u32,
    pub vertices: Vec<ModelVertex>,
    pub indices: Vec<u32>,
}
impl Material {
    pub fn destroy(self, device: &wgpu_hal::vulkan::Device) {
        unsafe {
            device.destroy_buffer(self.index_buffer);
            device.destroy_buffer(self.vertex_buffer);
            self.diffuse_texture.destroy(device)
        }
    }
}

#[derive(Copy, Clone)]
pub struct Bone {
    pub inv_bind_matrix: Mat4,
    pub transform_matrix: Mat4,
    pub parent: u32,
}

pub struct GltfModel {
    pub materials: Vec<Material>,
    pub skinned_vertices: Vec<Vec<ModelVertex>>,
    pub blas: BlasContainer,
    pub skeleton: Option<Vec<Bone>>,
}

impl GltfModel {
    pub fn update_blas(&self, device: &wgpu_hal::vulkan::Device, command_encoder: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for (u, i) in self.materials.iter().enumerate() {
                write_buffer(cast_slice(&self.skinned_vertices[u]), device, &i.vertex_buffer);
            }

            let blas_triangles: Vec<_> = self.materials.par_iter().map(|x| resources::to_blas_triangles(x)).collect();
            command_encoder.build_acceleration_structures(
                1,
                once(wgpu_hal::BuildAccelerationStructureDescriptor {
                    entries: &wgpu_hal::AccelerationStructureEntries::Triangles(blas_triangles),
                    mode: wgpu_hal::AccelerationStructureBuildMode::Update,
                    flags: wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE
                        | wgpu_hal::AccelerationStructureBuildFlags::PREFER_FAST_TRACE
                        | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_COMPACTION,
                    source_acceleration_structure: None,
                    destination_acceleration_structure: &self.blas.blas,
                    scratch_buffer: &self.blas.scratch,
                    scratch_buffer_offset: 0,
                }),
            )
        }
    }
    pub fn update_skinned_model(&mut self) {
        let skel = self.skeleton.clone().unwrap();
        let matr: Vec<_> = self
            .materials
            .par_iter()
            .map(|y| {
                y.vertices
                    .par_iter()
                    .map(|x| {
                        let mut y = *x;
                        let trans = x.weights[0] * joint_matrix(skel[x.joints[0] as usize])
                            + x.weights[1] * joint_matrix(skel[x.joints[1] as usize])
                            + x.weights[2] * joint_matrix(skel[x.joints[2] as usize])
                            + x.weights[3] * joint_matrix(skel[x.joints[3] as usize]);
                        let fin_p = trans * Vec3::from_array(x.position).extend(1.0);
                        let fin_n = mat4_to_mat3(trans) * Vec3::from_array(x.normal);
                        y.position = fin_p.truncate().to_array();
                        y.normal = fin_n.to_array();
                        y
                    })
                    .collect::<Vec<ModelVertex>>()
            })
            .collect();
        self.skinned_vertices = matr
    }
}

#[inline]
fn mat4_to_mat3(x: Mat4) -> Mat3 {
    Mat3::from_cols_array_2d(&[
        x.x_axis.truncate().to_array(),
        x.y_axis.truncate().to_array(),
        x.z_axis.truncate().to_array(),
    ])
}
