use bytemuck::cast_slice;
use egui::{ImageData, TextureId, TexturesDelta};
use std::borrow::Cow;
use std::collections::HashMap;
use std::iter::once;
use std::mem::{replace, size_of, size_of_val};
use wgpu_hal::{CommandEncoder, Device};
use wgpu_types::Extent3d;
use winit::dpi::PhysicalSize;

use crate::texture::Texture;
use crate::{submit_and_wait, write_buffer, write_texture};

const WIDTH: u32 = 900;
const HEIGHT: u32 = 900;

pub struct Gui {
    pub vertex_buffers: HashMap<u32, (wgpu_hal::vulkan::Buffer, u32)>,
    pub index_buffers: HashMap<u32, (wgpu_hal::vulkan::Buffer, u32)>,
    pub egui_data: Vec<egui::epaint::Mesh>,
    pub shader: wgpu_hal::vulkan::ShaderModule,
    pub egui_rpl: wgpu_hal::vulkan::PipelineLayout,
    pub egui_rp: wgpu_hal::vulkan::RenderPipeline,
    pub dest_texture: Texture,
    pub uniform_buffer: wgpu_hal::vulkan::Buffer,
    pub bind_groups: HashMap<TextureId, wgpu_hal::vulkan::BindGroup>,
    pub uniform_bind_group: wgpu_hal::vulkan::BindGroup,
    pub textures: HashMap<TextureId, Texture>,
    pub mesh_texture_ids: HashMap<u32, TextureId>,
    pub texture_bgl: wgpu_hal::vulkan::BindGroupLayout,
    pub uniform_bgl: wgpu_hal::vulkan::BindGroupLayout,
    pub gui_sampler: wgpu_hal::vulkan::Sampler,
}
impl Gui {
    pub fn init(
        device: &wgpu_hal::vulkan::Device,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        queue: &mut wgpu_hal::vulkan::Queue,
    ) -> Self {
        unsafe {
            let (vertex_buffers, index_buffers) = (HashMap::new(), HashMap::new());
            let shader_data = {
                let source = include_str!("../shaders/egui.wgsl");
                let module = naga::front::wgsl::Frontend::new().parse(source).unwrap();
                let info = naga::valid::Validator::new(naga::valid::ValidationFlags::all(), naga::valid::Capabilities::default())
                    .validate(&module)
                    .unwrap();
                wgpu_hal::NagaShader {
                    module: Cow::Owned(module),
                    info,
                    debug_source: None,
                }
            };
            let shader = device
                .create_shader_module(
                    &wgpu_hal::ShaderModuleDescriptor {
                        label: "EGUI Shader".into(),
                        runtime_checks: false,
                    },
                    wgpu_hal::ShaderInput::Naga(shader_data),
                )
                .unwrap();
            let uniform_bgl = device
                .create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                    label: "egui bind group layout".into(),
                    flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                    entries: &[wgpu_types::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu_types::ShaderStages::VERTEX,
                        ty: wgpu_types::BindingType::Buffer {
                            ty: wgpu_types::BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            min_binding_size: Some((size_of::<[f32; 4]>() as u64).try_into().unwrap()),
                        },
                        count: None,
                    }],
                })
                .unwrap();
            let texture_bgl = device
                .create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                    label: "egui bind group layout".into(),
                    flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                    entries: &[
                        wgpu_types::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu_types::ShaderStages::FRAGMENT,
                            ty: wgpu_types::BindingType::Texture {
                                sample_type: wgpu_types::TextureSampleType::Float { filterable: true },
                                view_dimension: wgpu_types::TextureViewDimension::D2,
                                multisampled: false,
                            },
                            count: None,
                        },
                        wgpu_types::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu_types::ShaderStages::FRAGMENT,
                            ty: wgpu_types::BindingType::Sampler(wgpu_types::SamplerBindingType::Filtering),
                            count: None,
                        },
                    ],
                })
                .unwrap();
            let egui_rpl = device
                .create_pipeline_layout(&wgpu_hal::PipelineLayoutDescriptor {
                    label: "EGUI RENDE PIPELINE Layout".into(),
                    flags: wgpu_hal::PipelineLayoutFlags::empty(),
                    bind_group_layouts: &[&uniform_bgl, &texture_bgl],
                    push_constant_ranges: &[],
                })
                .unwrap();
            let egui_rp = device
                .create_render_pipeline(&wgpu_hal::RenderPipelineDescriptor {
                    label: "Egui render pipeline".into(),
                    layout: &egui_rpl,
                    vertex_buffers: &[wgpu_hal::VertexBufferLayout {
                        array_stride: (size_of::<f32>() * 5) as _,
                        step_mode: wgpu_types::VertexStepMode::Vertex,
                        attributes: &[
                            wgpu_types::VertexAttribute {
                                format: wgpu_types::VertexFormat::Float32x2,
                                offset: 0,
                                shader_location: 0,
                            },
                            wgpu_types::VertexAttribute {
                                format: wgpu_types::VertexFormat::Float32x2,
                                offset: (size_of::<f32>() * 2) as _,
                                shader_location: 1,
                            },
                            wgpu_types::VertexAttribute {
                                format: wgpu_types::VertexFormat::Uint32,
                                offset: (size_of::<f32>() * 4) as _,
                                shader_location: 2,
                            },
                        ],
                    }],
                    vertex_stage: wgpu_hal::ProgrammableStage {
                        module: &shader,
                        entry_point: "vs_main",
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                        vertex_pulling_transform: false,
                    },
                    primitive: wgpu_types::PrimitiveState {
                        topology: wgpu_types::PrimitiveTopology::TriangleList,
                        strip_index_format: None,
                        front_face: Default::default(),
                        cull_mode: None,
                        unclipped_depth: false,
                        polygon_mode: Default::default(),
                        conservative: false,
                    },
                    depth_stencil: None,
                    multisample: Default::default(),
                    fragment_stage: Some(wgpu_hal::ProgrammableStage {
                        module: &shader,
                        entry_point: "fs_main_linear_framebuffer",
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                        vertex_pulling_transform: false,
                    }),
                    color_targets: &[Some(wgpu_types::ColorTargetState {
                        format: wgpu_types::TextureFormat::Rgba8UnormSrgb,
                        blend: Some(wgpu_types::BlendState {
                            color: wgpu_types::BlendComponent {
                                src_factor: wgpu_types::BlendFactor::One,
                                dst_factor: wgpu_types::BlendFactor::OneMinusSrcAlpha,
                                operation: wgpu_types::BlendOperation::Add,
                            },
                            alpha: wgpu_types::BlendComponent {
                                src_factor: wgpu_types::BlendFactor::OneMinusDstAlpha,
                                dst_factor: wgpu_types::BlendFactor::One,
                                operation: wgpu_types::BlendOperation::Add,
                            },
                        }),
                        write_mask: wgpu_types::ColorWrites::ALL,
                    })],
                    multiview: None,
                    cache: None,
                })
                .unwrap();
            let color_target = Texture::from_size(
                device,
                queue,
                (WIDTH, HEIGHT),
                None,
                command_encoder,
                Some(wgpu_hal::TextureUses::COLOR_TARGET | wgpu_hal::TextureUses::RESOURCE),
                wgpu_types::TextureFormat::Rgba8UnormSrgb,
            )
            .unwrap();
            let sampler = Texture::create_sampler(device);
            {
                command_encoder.begin_encoding(None).unwrap();
                command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                    texture: &color_target.texture,
                    range: Default::default(),
                    usage: wgpu_hal::TextureUses::UNINITIALIZED..wgpu_hal::TextureUses::RESOURCE,
                }));
                submit_and_wait(command_encoder, device, queue).unwrap();
            }
            let uniform_buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: "Egui_uniform".into(),
                    size: (size_of::<f32>() * 4) as _,
                    usage: wgpu_hal::BufferUses::UNIFORM | wgpu_hal::BufferUses::MAP_WRITE,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            write_buffer(cast_slice(&[WIDTH as f32, HEIGHT as f32, 0.0, 0.0]), device, &uniform_buffer);
            let uniform_bind_group = device
                .create_bind_group(&wgpu_hal::BindGroupDescriptor {
                    label: "Uniform EGUI bind group".into(),
                    layout: &uniform_bgl,
                    buffers: &[wgpu_hal::BufferBinding {
                        buffer: &uniform_buffer,
                        offset: 0,
                        size: None,
                    }],
                    samplers: &[],
                    textures: &[],
                    entries: &[wgpu_hal::BindGroupEntry {
                        binding: 0,
                        resource_index: 0,
                        count: 1,
                    }],
                    acceleration_structures: &[],
                })
                .unwrap();
            let texture_bind_groups = HashMap::new();
            let textures = HashMap::new();
            let mesh_texture_ids = HashMap::new();
            Self {
                vertex_buffers,
                index_buffers,
                mesh_texture_ids,
                egui_data: vec![],
                shader,
                egui_rpl,
                egui_rp,
                dest_texture: color_target,
                uniform_buffer,
                bind_groups: texture_bind_groups,
                texture_bgl,
                textures,
                uniform_bind_group,
                uniform_bgl,
                gui_sampler: sampler,
            }
        }
    }
    pub fn update(
        &mut self,
        device: &wgpu_hal::vulkan::Device,
        textures_delta: TexturesDelta,
        paintjobs: Vec<egui::epaint::ClippedPrimitive>,
        queue: &mut wgpu_hal::vulkan::Queue,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    ) {
        unsafe {
            for (i, u) in paintjobs.iter().enumerate() {
                if let egui::epaint::Primitive::Mesh(x) = &u.primitive {
                    let size_of_vtx = size_of_val(x.vertices.as_slice()) as u32;
                    if let Some(current_vb_container) = self.vertex_buffers.get_mut(&(i as u32)) {
                        if current_vb_container.1 < size_of_vtx {
                            let new_vtx_buffer = device
                                .create_buffer(&wgpu_hal::BufferDescriptor {
                                    label: "gui_Vbuffer".into(),
                                    size: size_of_vtx as _,
                                    usage: wgpu_hal::BufferUses::VERTEX | wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::COPY_DST,
                                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                                })
                                .unwrap();
                            let old_buffer = replace(&mut current_vb_container.0, new_vtx_buffer);
                            device.destroy_buffer(old_buffer);
                            current_vb_container.1 = size_of_vtx;
                        }
                    } else {
                        let new_vtx_buffer = device
                            .create_buffer(&wgpu_hal::BufferDescriptor {
                                label: "gui_Vbuffer".into(),
                                size: size_of_vtx as _,
                                usage: wgpu_hal::BufferUses::VERTEX | wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::COPY_DST,
                                memory_flags: wgpu_hal::MemoryFlags::empty(),
                            })
                            .unwrap();
                        self.vertex_buffers.insert(i as _, (new_vtx_buffer, size_of_vtx));
                    }
                    write_buffer(
                        cast_slice(x.vertices.as_slice()),
                        device,
                        &self.vertex_buffers.get(&(i as _)).expect("Buffer did not exist").0,
                    );

                    let size_of_ind = size_of_val(x.indices.as_slice()) as u32;
                    if let Some(current_ib_container) = self.index_buffers.get_mut(&(i as u32)) {
                        if current_ib_container.1 < size_of_ind {
                            let new_ind_buffer = device
                                .create_buffer(&wgpu_hal::BufferDescriptor {
                                    label: "gui_Ibuffer".into(),
                                    size: size_of_ind as _,
                                    usage: wgpu_hal::BufferUses::INDEX | wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::COPY_DST,
                                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                                })
                                .unwrap();
                            let old_buffer = replace(&mut current_ib_container.0, new_ind_buffer);
                            device.destroy_buffer(old_buffer);
                            current_ib_container.1 = size_of_ind;
                        }
                    } else {
                        let new_ind_buffer = device
                            .create_buffer(&wgpu_hal::BufferDescriptor {
                                label: "gui_Ibuffer".into(),
                                size: size_of_ind as _,
                                usage: wgpu_hal::BufferUses::INDEX | wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::COPY_DST,
                                memory_flags: wgpu_hal::MemoryFlags::empty(),
                            })
                            .unwrap();
                        self.index_buffers.insert(i as u32, (new_ind_buffer, size_of_ind));
                    }
                    write_buffer(
                        cast_slice(x.indices.as_slice()),
                        device,
                        &self.index_buffers.get(&(i as _)).expect("Buffer did not exist").0,
                    );

                    self.mesh_texture_ids.insert(i as _, x.texture_id);
                } else {
                    let _callback = &u.primitive;
                }
            }
            for i in textures_delta.set.iter() {
                let size = Extent3d {
                    width: i.1.image.width() as _,
                    height: i.1.image.height() as _,
                    depth_or_array_layers: 1,
                };
                let pixels_data = match &i.1.image {
                    ImageData::Color(x) => x.pixels.clone(),
                    ImageData::Font(x) => x.srgba_pixels(None).collect::<Vec<_>>(),
                };
                if let Some(x) = i.1.pos {
                    write_texture(
                        cast_slice(pixels_data.as_slice()),
                        device,
                        self.textures
                            .get(&i.0)
                            .expect("Tried to update a texture that has not been allocated yet."),
                        wgpu_types::Origin2d {
                            x: x[0] as u32,
                            y: x[1] as u32,
                        },
                        command_encoder,
                        queue,
                    )
                } else {
                    let texture = Texture::from_size(
                        device,
                        queue,
                        (size.width, size.height),
                        None,
                        command_encoder,
                        None,
                        wgpu_types::TextureFormat::Rgba8UnormSrgb,
                    )
                    .unwrap();
                    let bind_group = device
                        .create_bind_group(&wgpu_hal::BindGroupDescriptor {
                            label: "Texture EGUI bind group".into(),
                            layout: &self.texture_bgl,
                            buffers: &[],
                            samplers: &[&self.gui_sampler],
                            textures: &[wgpu_hal::TextureBinding {
                                view: &texture.view,
                                usage: wgpu_hal::TextureUses::RESOURCE,
                            }],
                            entries: &[
                                wgpu_hal::BindGroupEntry {
                                    binding: 0,
                                    resource_index: 0,
                                    count: 1,
                                },
                                wgpu_hal::BindGroupEntry {
                                    binding: 1,
                                    resource_index: 0,
                                    count: 1,
                                },
                            ],
                            acceleration_structures: &[],
                        })
                        .unwrap();
                    write_texture(
                        cast_slice(pixels_data.as_slice()),
                        device,
                        &texture,
                        wgpu_types::Origin2d::ZERO,
                        command_encoder,
                        queue,
                    );
                    self.textures.insert(i.0, texture);
                    self.bind_groups.insert(i.0, bind_group);
                }
            }
            for i in textures_delta.free {
                device.destroy_bind_group(self.bind_groups.remove(&i).unwrap());
                self.textures.remove(&i).unwrap().destroy(device);
            }
        }
    }
    pub fn render(&mut self, command_encoder: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &self.dest_texture.texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::RESOURCE..wgpu_hal::TextureUses::COLOR_TARGET,
            }));
            command_encoder.begin_render_pass(&wgpu_hal::RenderPassDescriptor {
                label: "EGUI renderpass".into(),
                extent: Extent3d {
                    width: self.dest_texture.size.width,
                    height: self.dest_texture.size.height,
                    depth_or_array_layers: 1,
                },
                sample_count: 1,
                color_attachments: &[Some(wgpu_hal::ColorAttachment {
                    target: wgpu_hal::Attachment {
                        view: &self.dest_texture.view,
                        usage: wgpu_hal::TextureUses::COLOR_TARGET,
                    },
                    resolve_target: None,
                    ops: wgpu_hal::AttachmentOps::STORE,
                    clear_value: wgpu_types::Color::TRANSPARENT,
                })],
                depth_stencil_attachment: None,
                multiview: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });
            command_encoder.set_render_pipeline(&self.egui_rp);
            command_encoder.set_bind_group(&self.egui_rpl, 0, &self.uniform_bind_group, &[]);
            let mut t = self.mesh_texture_ids.iter().collect::<Vec<_>>();
            t.sort_unstable();
            for (u, i) in t {
                command_encoder.set_bind_group(&self.egui_rpl, 1, self.bind_groups.get(i).unwrap(), &[]);
                command_encoder.set_vertex_buffer(
                    0,
                    wgpu_hal::BufferBinding {
                        buffer: &self.vertex_buffers.get(u).unwrap().0,
                        offset: 0,
                        size: None,
                    },
                );
                command_encoder.set_index_buffer(
                    wgpu_hal::BufferBinding {
                        buffer: &self.index_buffers.get(u).unwrap().0,
                        offset: 0,
                        size: None,
                    },
                    wgpu_types::IndexFormat::Uint32,
                );
                command_encoder.draw_indexed(0, self.index_buffers.get(u).unwrap().1 / 4 /*size_of::<u32>*/, 0, 0, 1)
            }
            command_encoder.end_render_pass();
            command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &self.dest_texture.texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::COLOR_TARGET..wgpu_hal::TextureUses::RESOURCE,
            }));
            for i in self.vertex_buffers.values() {
                command_encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
                    buffer: &i.0,
                    usage: wgpu_hal::BufferUses::VERTEX..wgpu_hal::BufferUses::COPY_DST,
                }));
                command_encoder.clear_buffer(&i.0, 0..i.1 as _);
                command_encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
                    buffer: &i.0,
                    usage: wgpu_hal::BufferUses::COPY_DST..wgpu_hal::BufferUses::VERTEX,
                }));
            }
            for i in self.index_buffers.values() {
                command_encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
                    buffer: &i.0,
                    usage: wgpu_hal::BufferUses::INDEX..wgpu_hal::BufferUses::COPY_DST,
                }));
                command_encoder.clear_buffer(&i.0, 0..i.1 as _);
                command_encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
                    buffer: &i.0,
                    usage: wgpu_hal::BufferUses::COPY_DST..wgpu_hal::BufferUses::INDEX,
                }));
            }
            self.mesh_texture_ids.clear();
        }
    }
    pub fn destroy(self, device: &wgpu_hal::vulkan::Device) {
        unsafe {
            for (_u, i) in self.vertex_buffers {
                device.destroy_buffer(i.0)
            }
            for (_u, i) in self.index_buffers {
                device.destroy_buffer(i.0)
            }
            for (_u, i) in self.bind_groups {
                device.destroy_bind_group(i)
            }
            for (_u, i) in self.textures {
                i.destroy(device)
            }
            device.destroy_buffer(self.uniform_buffer);
            self.dest_texture.destroy(device);
            device.destroy_sampler(self.gui_sampler);
            device.destroy_render_pipeline(self.egui_rp);
            device.destroy_pipeline_layout(self.egui_rpl);
            device.destroy_bind_group_layout(self.texture_bgl);
            device.destroy_bind_group(self.uniform_bind_group);
            device.destroy_bind_group_layout(self.uniform_bgl);
            device.destroy_shader_module(self.shader);
        }
    }
    pub fn resize(
        &mut self,
        u: PhysicalSize<u32>,
        device: &wgpu_hal::vulkan::Device,
        queue: &mut wgpu_hal::vulkan::Queue,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    ) {
        write_buffer(
            cast_slice(&[u.width as f32, u.height as f32, 0.0, 0.0]),
            device,
            &self.uniform_buffer,
        );
        self.dest_texture.resize(
            device,
            queue,
            command_encoder,
            Extent3d {
                width: u.width,
                height: u.height,
                depth_or_array_layers: 1,
            },
        );

        unsafe {
            command_encoder.begin_encoding(None).unwrap();
            command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &self.dest_texture.texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::UNINITIALIZED..wgpu_hal::TextureUses::RESOURCE,
            }));
            submit_and_wait(command_encoder, device, queue).unwrap();
        }
    }
}
