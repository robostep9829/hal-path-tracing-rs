use std::borrow::Borrow;
use std::f64::consts::PI;
use std::fmt::Debug;
use std::iter::once;
use std::mem::{replace, size_of, size_of_val};
use std::rc::Rc;

use bytemuck::cast_slice;
use glam::{Affine3A, Mat4, Quat, Vec3A};
use raw_window_handle::{HasDisplayHandle, HasWindowHandle};
use wgpu_hal::{Adapter, BufferUses, CommandEncoder, ComputePassDescriptor, Device, FenceValue, Instance, Queue, Surface};
use wgpu_types as wgt;
use wgpu_types::{Extent3d, ShaderStages};
use winit::dpi::PhysicalSize;
use winit::event::{ElementState, MouseButton};

use crate::filtering::Filtering;
use crate::g_buffers::HelperTextures;
use crate::gui::Gui;
use crate::model::{Bone, GltfModel};
use crate::post_processing::PostProcessing;
use crate::resources::{load_gltf, make_spirv_raw, ModelArena};
use crate::texture::Texture;
use crate::{camera, resources, submit_and_wait, write_buffer, CameraUniform, ObjInstance, TlasContainer, NUM_INSTANCES_PER_ROW};

#[derive(Debug)]
pub struct AppState {
    pub custom_parameter: [f32; 16],
    pub mouse_pressed: bool,
    pub selected_index: u32,
    pub return_from_shader: [u8; 16 * size_of::<u32>()],
    pub interacting_with_ui: bool,
    pub skeletal_bone: usize,
    pub is_resize_needed: Option<PhysicalSize<u32>>,
}

pub struct App {
    pub instance: wgpu_hal::vulkan::Instance,
    pub window: winit::window::Window,
    pub surface: wgpu_hal::vulkan::Surface,
    pub adapter: wgpu_hal::ExposedAdapter<wgpu_hal::vulkan::Api>,
    pub device: Rc<wgpu_hal::vulkan::Device>,
    pub queue: wgpu_hal::vulkan::Queue,
    pub surface_config: wgpu_hal::SurfaceConfiguration,
    pub frag_shader: wgpu_hal::vulkan::ShaderModule,
    pub rp_layout: wgpu_hal::vulkan::PipelineLayout,
    pub color_target: wgt::ColorTargetState,
    pub render_pipeline: wgpu_hal::vulkan::ComputePipeline,
    pub surface_capabilities: wgpu_hal::SurfaceCapabilities,
    pub encoder: wgpu_hal::vulkan::CommandEncoder,
    pub camera: camera::Camera,
    pub projection: camera::Projection,
    pub camera_uniform: CameraUniform,
    pub camera_buffer: wgpu_hal::vulkan::Buffer,
    pub camera_bgl: wgpu_hal::vulkan::BindGroupLayout,
    pub camera_bind_group: wgpu_hal::vulkan::BindGroup,
    pub camera_controller: camera::CameraController,
    pub instances: Vec<ObjInstance>,
    pub custom_buffer: wgpu_hal::vulkan::Buffer,
    pub other_models: ModelArena,
    pub rt_bgl: wgpu_hal::vulkan::BindGroupLayout,
    pub rt_bind_group: wgpu_hal::vulkan::BindGroup,
    pub dest_texture: wgpu_hal::vulkan::Texture,
    pub dest_texture_view: wgpu_hal::vulkan::TextureView,
    pub main_bind_group: wgpu_hal::vulkan::BindGroup,
    pub rng_buffer: wgpu_hal::vulkan::Buffer,
    pub tlas: TlasContainer,
    pub bgl: wgpu_hal::vulkan::BindGroupLayout,
    pub egui_state: egui_winit::State,
    pub gui_infra: Gui,
    pub helper_textures: HelperTextures,
    pub state: AppState,
    pub gltf_scenes: Vec<GltfModel>,
    pub post_process_infra: PostProcessing,
    pub world_sampler: wgpu_hal::vulkan::Sampler,
    pub pf_camera_uniform: wgpu_hal::vulkan::Buffer,
    pub filtering_infra: Filtering,
}

impl App {
    pub(crate) async fn init(event_loop: &winit::event_loop::EventLoop<()>) -> Self {
        unsafe {
            let egui_ctx = egui::Context::default();
            let instance = wgpu_hal::vulkan::Instance::init(&wgpu_hal::InstanceDescriptor {
                name: "Instance1",
                flags: wgt::InstanceFlags::VALIDATION | wgt::InstanceFlags::GPU_BASED_VALIDATION,
                dx12_shader_compiler: Default::default(),
                gles_minor_version: Default::default(),
            })
            .unwrap();
            let egui_viewport_builder = egui::ViewportBuilder {
                transparent: Some(true),
                ..Default::default()
            };
            let egui_window_builder = egui_winit::create_winit_window_builder(&egui_ctx, event_loop, egui_viewport_builder);
            let window = egui_window_builder
                .with_resizable(true)
                .with_title("AMIGUS")
                .with_inner_size(PhysicalSize::new(900, 900))
                .build(event_loop)
                .unwrap();
            let egui_viewport = egui_ctx.viewport_id();
            let egui_state = egui_winit::State::new(egui_ctx, egui_viewport, &window, None, None);
            let surface = instance
                .create_surface(window.display_handle().unwrap().as_raw(), window.window_handle().unwrap().as_raw())
                .unwrap();
            let adapter = {
                let mut x = instance.enumerate_adapters(None);
                x.swap_remove(0)
            };
            let surface_capabilities = adapter.adapter.surface_capabilities(&surface).unwrap();
            let surface_format = surface_capabilities.formats.iter().find(|x| x.is_srgb()).unwrap();
            let wgpu_hal::OpenDevice { device, mut queue } = adapter
                .adapter
                .open(
                    adapter.features | wgt::Features::BUFFER_BINDING_ARRAY,
                    &wgt::Limits::default(),
                    &wgt::MemoryHints::Performance,
                )
                .unwrap();
            let device = Rc::new(device);
            let surface_config = wgpu_hal::SurfaceConfiguration {
                maximum_frame_latency: *surface_capabilities.maximum_frame_latency.start(),
                present_mode: wgt::PresentMode::Fifo,
                composite_alpha_mode: surface_capabilities.composite_alpha_modes[0],
                format: *surface_format,
                extent: Extent3d {
                    width: window.inner_size().width,
                    height: window.inner_size().height,
                    depth_or_array_layers: 1,
                },
                usage: wgpu_hal::TextureUses::COLOR_TARGET | wgpu_hal::TextureUses::COPY_DST,
                view_formats: vec![],
            };
            surface.configure(&device, &surface_config).unwrap();
            let mut command_encoder = device
                .create_command_encoder(&wgpu_hal::CommandEncoderDescriptor {
                    label: Some("CommandEncoder"),
                    queue: &queue,
                })
                .unwrap();

            let mut tlas = {
                let tlas = device
                    .create_acceleration_structure(&wgpu_hal::AccelerationStructureDescriptor {
                        label: Some("MAIN TLAS"),
                        size: 1,
                        format: wgpu_hal::AccelerationStructureFormat::TopLevel,
                    })
                    .unwrap();
                let top_scratch_buffer = device
                    .create_buffer(&wgpu_hal::BufferDescriptor {
                        label: Some("scratch buffer"),
                        size: device
                            .get_acceleration_structure_build_sizes(&wgpu_hal::GetAccelerationStructureBuildSizesDescriptor {
                                entries: &wgpu_hal::AccelerationStructureEntries::Instances(wgpu_hal::AccelerationStructureInstances {
                                    buffer: None,
                                    offset: 0,
                                    count: 0,
                                }),
                                flags: wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE,
                            })
                            .build_scratch_size,
                        usage: BufferUses::ACCELERATION_STRUCTURE_SCRATCH,
                        memory_flags: wgpu_hal::MemoryFlags::empty(),
                    })
                    .unwrap();
                let instances_buffer = device
                    .create_buffer(&wgpu_hal::BufferDescriptor {
                        label: Some("instances_buffer"),
                        size: 1,
                        usage: BufferUses::MAP_WRITE | BufferUses::TOP_LEVEL_ACCELERATION_STRUCTURE_INPUT,
                        memory_flags: wgpu_hal::MemoryFlags::TRANSIENT | wgpu_hal::MemoryFlags::PREFER_COHERENT,
                    })
                    .unwrap();
                TlasContainer {
                    tlas,
                    instances: vec![],
                    scratch: top_scratch_buffer,
                    instances_buffer,
                }
            };
            let instances = (0..NUM_INSTANCES_PER_ROW)
                .flat_map(|z| {
                    (0..NUM_INSTANCES_PER_ROW).map(move |x| {
                        let x = SPACE_BETWEEN * (x as f32 - NUM_INSTANCES_PER_ROW as f32 / 2.0);
                        let z = SPACE_BETWEEN * (z as f32 - NUM_INSTANCES_PER_ROW as f32 / 2.0);

                        let position = Vec3A::new(x, 0.0, z);

                        let rotation = if position.length() == 0.0 {
                            Quat::from_axis_angle(Vec3A::new(0.0, 0.0, 1.0).into(), (45.0 * PI / 180.0) as f32)
                        } else {
                            Quat::from_axis_angle(position.normalize().into(), (45.0 * PI / 180.0) as f32)
                        };
                        ObjInstance {
                            position,
                            rotation,
                            scale: Vec3A::new(1.0, 1.0, 1.0),
                        }
                    })
                })
                .collect::<Vec<_>>();

            let mut other_models = ModelArena::new();
            resources::load_model(
                "cube.obj",
                &device,
                &mut queue,
                &mut command_encoder,
                &instances,
                &mut tlas,
                &mut other_models,
            )
            .await
            .unwrap();
            resources::load_model(
                "sky_sphere.obj",
                &device,
                &mut queue,
                &mut command_encoder,
                &[ObjInstance {
                    position: Vec3A::new(0.0, 5.5, 0.0),
                    rotation: Quat::default(),
                    scale: Vec3A::new(1.0, 1.0, 1.0),
                }],
                &mut tlas,
                &mut other_models,
            )
            .await
            .unwrap();
            resources::load_model(
                "plane_floor.obj",
                &device,
                &mut queue,
                &mut command_encoder,
                &[ObjInstance {
                    position: Vec3A::new(0.0, -2.5, 0.0),
                    rotation: Quat::default(),
                    scale: Vec3A::new(1.0, 1.0, 1.0),
                }],
                &mut tlas,
                &mut other_models,
            )
            .await
            .unwrap();
            resources::load_model(
                "obscure_test.obj",
                &device,
                &mut queue,
                &mut command_encoder,
                &[ObjInstance {
                    position: Vec3A::new(0.0, 2.5, 0.0),
                    rotation: Quat::default(),
                    scale: Vec3A::new(1.0, 1.0, 1.0),
                }],
                &mut tlas,
                &mut other_models,
            )
            .await
            .unwrap();
            resources::load_model(
                "light_sphere.obj",
                &device,
                &mut queue,
                &mut command_encoder,
                &[ObjInstance {
                    position: Vec3A::new(5.0, 2.5, 0.0),
                    rotation: Quat::default(),
                    scale: Vec3A::new(1.0, 1.0, 1.0),
                }],
                &mut tlas,
                &mut other_models,
            )
            .await
            .unwrap();
            // resources::load_model(
            //     "Sponza/sponza.obj",
            //     &device,
            //     &mut queue,
            //     &mut command_encoder,
            //     &[ObjInstance {
            //         position: Vec3A::new(5.0, 2.5, 0.0),
            //         rotation: Quat::default(),
            //         scale: Vec3A::new(1.0, 1.0, 1.0),
            //     }],
            //     &mut tlas,
            //     &mut other_models,
            // )
            // .await
            // .unwrap();

            let gltf_test = load_gltf(&device, &mut queue, &mut command_encoder, &mut tlas, other_models.rt_index_offset());

            let mut material_count: u32 = 0;
            for o in other_models.models.iter() {
                for _i in o.materials.iter() {
                    material_count += 1
                }
            }
            material_count += 1;

            let bgl = device
                .create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                    label: Some("Bind Group layout"),
                    flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                    entries: &[
                        wgt::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: ShaderStages::COMPUTE,
                            ty: wgt::BindingType::Texture {
                                sample_type: wgt::TextureSampleType::Float { filterable: true },
                                view_dimension: wgt::TextureViewDimension::D2,
                                multisampled: false,
                            },
                            count: Some(material_count.try_into().unwrap()),
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: ShaderStages::COMPUTE,
                            ty: wgt::BindingType::Sampler(wgt::SamplerBindingType::Filtering),
                            count: Some(material_count.try_into().unwrap()),
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: ShaderStages::COMPUTE,
                            ty: wgt::BindingType::Buffer {
                                ty: wgt::BufferBindingType::Storage { read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: Some(material_count.try_into().unwrap()),
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 3,
                            visibility: ShaderStages::COMPUTE,
                            ty: wgt::BindingType::Buffer {
                                ty: wgt::BufferBindingType::Storage { read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: Some(material_count.try_into().unwrap()),
                        },
                    ],
                })
                .unwrap();
            let frag_shader = device
                .create_shader_module(
                    &wgpu_hal::ShaderModuleDescriptor {
                        label: Some("Fragment_Global"),
                        runtime_checks: false,
                    },
                    wgpu_hal::ShaderInput::SpirV(&make_spirv_raw(include_bytes!("../shaders/frag.spv"))),
                )
                .unwrap();
            let color_target = wgt::ColorTargetState {
                format: *surface_format,
                blend: None,
                write_mask: Default::default(),
            };
            let camera = camera::Camera::new((0.0, 5.0, 10.0), (-90.0f32).to_radians(), (-20.0f32).to_radians());
            let projection = camera::Projection::new(
                window.inner_size().width,
                window.inner_size().height,
                60.0f32.to_radians(),
                0.1,
                100.0,
            );
            let mut camera_uniform = CameraUniform::new();
            let camera_controller = camera::CameraController::new(4.0, 0.4);
            camera_uniform.update_view_proj(&camera, &projection);
            let camera_buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: Some("camera buffer"),
                    size: size_of_val(&camera_uniform) as u64,
                    usage: BufferUses::UNIFORM | BufferUses::MAP_WRITE | BufferUses::COPY_SRC,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            write_buffer(cast_slice(&[camera_uniform]), &device, &camera_buffer);
            let custom_buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: Some("Custom parameter"),
                    size: (size_of::<u32>() * 16) as u64,
                    usage: BufferUses::MAP_WRITE | BufferUses::STORAGE_READ_WRITE,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            write_buffer(cast_slice(&[0.0f32; 16]), &device, &custom_buffer);
            let rng_buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: Some("RNG Seed"),
                    size: size_of::<u32>() as u64,
                    usage: BufferUses::MAP_WRITE | BufferUses::UNIFORM,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            write_buffer(cast_slice(&[0u32]), &device, &rng_buffer);
            let pf_camera_buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: Some("camera buffer"),
                    size: size_of_val(&camera_uniform) as u64,
                    usage: BufferUses::UNIFORM | BufferUses::MAP_WRITE | BufferUses::COPY_DST,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            let camera_bgl = device
                .create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                    label: Some("Camera bgl"),
                    flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                    entries: &[
                        wgt::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                            ty: wgt::BindingType::Buffer {
                                ty: wgt::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                            ty: wgt::BindingType::Buffer {
                                ty: wgt::BufferBindingType::Storage { read_only: false },
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                            ty: wgt::BindingType::Buffer {
                                ty: wgt::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 3,
                            visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                            ty: wgt::BindingType::Buffer {
                                ty: wgt::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                    ],
                })
                .unwrap();
            let camera_bind_group = device
                .create_bind_group(&wgpu_hal::BindGroupDescriptor {
                    label: Some("Camera binding group"),
                    layout: &camera_bgl,
                    buffers: &[
                        wgpu_hal::BufferBinding {
                            buffer: &camera_buffer,
                            offset: 0,
                            size: None,
                        },
                        wgpu_hal::BufferBinding {
                            buffer: &custom_buffer,
                            offset: 0,
                            size: None,
                        },
                        wgpu_hal::BufferBinding {
                            buffer: &rng_buffer,
                            offset: 0,
                            size: None,
                        },
                        wgpu_hal::BufferBinding {
                            buffer: &pf_camera_buffer,
                            offset: 0,
                            size: None,
                        },
                    ],
                    samplers: &[],
                    textures: &[],
                    entries: &[
                        wgpu_hal::BindGroupEntry {
                            binding: 0,
                            resource_index: 0,
                            count: 1,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 1,
                            resource_index: 1,
                            count: 1,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 2,
                            resource_index: 2,
                            count: 1,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 3,
                            resource_index: 3,
                            count: 1,
                        },
                    ],
                    acceleration_structures: &[],
                })
                .unwrap();

            let rt_bgl = device
                .create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                    label: Some("RT bind group layout"),
                    flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                    entries: &[
                        wgt::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: ShaderStages::COMPUTE,
                            ty: wgt::BindingType::StorageTexture {
                                access: wgt::StorageTextureAccess::ReadWrite,
                                format: *surface_format,
                                view_dimension: wgt::TextureViewDimension::D2,
                            },
                            count: None,
                        },
                        wgt::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: ShaderStages::COMPUTE,
                            ty: wgt::BindingType::AccelerationStructure,
                            count: None,
                        },
                    ],
                })
                .unwrap();
            const SPACE_BETWEEN: f32 = 3.0;

            let g_buffers = HelperTextures::new(
                &device,
                &mut queue,
                &mut command_encoder,
                Extent3d {
                    width: 900,
                    height: 900,
                    depth_or_array_layers: 1,
                },
            );
            let rp_layout = device
                .create_pipeline_layout(&wgpu_hal::PipelineLayoutDescriptor {
                    label: Some("RP_Layout"),
                    flags: wgpu_hal::PipelineLayoutFlags::empty(),
                    bind_group_layouts: &[&bgl, &camera_bgl, &rt_bgl, &g_buffers.bgl],
                    push_constant_ranges: &[],
                })
                .unwrap();
            let render_pipeline = device
                .create_compute_pipeline(&wgpu_hal::ComputePipelineDescriptor {
                    label: Some("Compute Pipeline"),
                    layout: &rp_layout,
                    stage: wgpu_hal::ProgrammableStage {
                        module: &frag_shader,
                        entry_point: "main",
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                        vertex_pulling_transform: false,
                    },
                    cache: None,
                })
                .unwrap();
            let scene_texture = device
                .create_texture(&wgpu_hal::TextureDescriptor {
                    label: None,
                    size: Extent3d {
                        width: window.inner_size().width,
                        height: window.inner_size().height,
                        depth_or_array_layers: 1,
                    },
                    mip_level_count: 1,
                    sample_count: 1,
                    dimension: wgt::TextureDimension::D2,
                    format: *surface_format,
                    usage: wgpu_hal::TextureUses::COPY_SRC | wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::RESOURCE,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                    view_formats: vec![],
                })
                .unwrap();
            let scene_texture_view = device
                .create_texture_view(
                    &scene_texture,
                    &wgpu_hal::TextureViewDescriptor {
                        label: None,
                        format: *surface_format,
                        dimension: wgt::TextureViewDimension::D2,
                        usage: wgpu_hal::TextureUses::COPY_SRC
                            | wgpu_hal::TextureUses::STORAGE_READ_WRITE
                            | wgpu_hal::TextureUses::RESOURCE,
                        range: Default::default(),
                    },
                )
                .unwrap();
            let world_sampler = Texture::create_sampler(&device);
            let mut all_vtx_buf = other_models
                .models
                .iter()
                .map(|x| {
                    x.materials
                        .iter()
                        .map(|y| wgpu_hal::BufferBinding {
                            buffer: &y.vertex_buffer,
                            offset: 0,
                            size: None,
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
                .concat();
            all_vtx_buf.extend(
                gltf_test
                    .iter()
                    .map(|x| {
                        x.materials
                            .iter()
                            .map(|y| wgpu_hal::BufferBinding {
                                buffer: &y.vertex_buffer,
                                offset: 0,
                                size: None,
                            })
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>()
                    .concat(),
            );
            let mut all_ind_buf = other_models
                .models
                .iter()
                .map(|x| {
                    x.materials
                        .iter()
                        .map(|y| wgpu_hal::BufferBinding {
                            buffer: &y.index_buffer,
                            offset: 0,
                            size: None,
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
                .concat();
            all_ind_buf.extend(
                gltf_test
                    .iter()
                    .map(|x| {
                        x.materials
                            .iter()
                            .map(|y| wgpu_hal::BufferBinding {
                                buffer: &y.index_buffer,
                                offset: 0,
                                size: None,
                            })
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>()
                    .concat(),
            );
            let (ind_len, vtx_len) = (all_ind_buf.len(), all_vtx_buf.len());
            let all_bind_buf = [all_ind_buf, all_vtx_buf].concat();
            let mut all_tex = other_models
                .models
                .iter()
                .map(|x| {
                    x.materials
                        .iter()
                        .map(|y| wgpu_hal::TextureBinding {
                            view: &y.diffuse_texture.view,
                            usage: wgpu_hal::TextureUses::RESOURCE,
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
                .concat();
            all_tex.extend(
                gltf_test
                    .iter()
                    .map(|x| {
                        x.materials
                            .iter()
                            .map(|y| wgpu_hal::TextureBinding {
                                view: &y.diffuse_texture.view,
                                usage: wgpu_hal::TextureUses::RESOURCE,
                            })
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>()
                    .concat(),
            );
            let mut all_samp = other_models
                .models
                .iter()
                .map(|x| x.materials.iter().map(|_y| &world_sampler).collect::<Vec<_>>())
                .collect::<Vec<_>>()
                .concat();
            all_samp.extend(
                gltf_test
                    .iter()
                    .map(|x| x.materials.iter().map(|_y| &world_sampler).collect::<Vec<_>>())
                    .collect::<Vec<_>>()
                    .concat(),
            );
            let main_bind_group = device
                .create_bind_group(&wgpu_hal::BindGroupDescriptor {
                    label: None,
                    layout: &bgl,
                    buffers: &all_bind_buf,
                    samplers: &all_samp,
                    textures: &all_tex,
                    entries: &[
                        wgpu_hal::BindGroupEntry {
                            binding: 0,
                            resource_index: 0,
                            count: all_tex.len() as u32,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 1,
                            resource_index: 0,
                            count: all_tex.len() as u32,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 2,
                            resource_index: 0,
                            count: ind_len as u32,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 3,
                            resource_index: ind_len as u32,
                            count: vtx_len as u32,
                        },
                    ],
                    acceleration_structures: &[],
                })
                .unwrap();

            let gui_infra = Gui::init(&device, &mut command_encoder, &mut queue);

            let rt_bind_group = device
                .create_bind_group(&wgpu_hal::BindGroupDescriptor {
                    label: Some("RT bind group"),
                    layout: &rt_bgl,
                    buffers: &[],
                    samplers: &[],
                    textures: &[wgpu_hal::TextureBinding {
                        view: &scene_texture_view,
                        usage: wgpu_hal::TextureUses::COPY_SRC | wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    }],
                    entries: &[
                        wgpu_hal::BindGroupEntry {
                            binding: 0,
                            resource_index: 0,
                            count: 1,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 1,
                            resource_index: 0,
                            count: 1,
                        },
                    ],
                    acceleration_structures: &[&tlas.tlas],
                })
                .unwrap();
            let post_process_infra = PostProcessing::new(
                &device,
                *surface_format,
                &scene_texture_view,
                &gui_infra.dest_texture.view,
                &g_buffers,
                &camera_bgl,
                &mut command_encoder,
                &mut queue,
                Extent3d {
                    width: 900,
                    height: 900,
                    depth_or_array_layers: 1,
                },
            );
            let filtering_infra = Filtering::new(&device, &post_process_infra, &g_buffers, &camera_bgl);

            command_encoder.begin_encoding(None).unwrap();
            command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &scene_texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::UNINITIALIZED..wgpu_hal::TextureUses::STORAGE_READ_WRITE,
            }));
            let _ = submit_and_wait(&mut command_encoder, &device, &queue);

            Self {
                instance,
                window,
                surface,
                adapter,
                device,
                queue,
                surface_config,
                frag_shader,
                rp_layout,
                color_target,
                render_pipeline,
                surface_capabilities,
                encoder: command_encoder,
                camera,
                projection,
                camera_uniform,
                pf_camera_uniform: pf_camera_buffer,
                camera_buffer,
                camera_bgl,
                camera_bind_group,
                camera_controller,
                instances,
                custom_buffer,
                other_models,
                gltf_scenes: gltf_test,
                rt_bgl,
                rt_bind_group,
                dest_texture: scene_texture,
                dest_texture_view: scene_texture_view,
                bgl,
                main_bind_group,
                rng_buffer,
                tlas,
                egui_state,
                gui_infra,
                helper_textures: g_buffers,
                state: Default::default(),
                post_process_infra,
                world_sampler,
                filtering_infra,
            }
        }
    }
    pub(crate) fn input(&mut self, event: &winit::event::WindowEvent) -> bool {
        match event {
            winit::event::WindowEvent::KeyboardInput { event, .. } => match event.physical_key {
                winit::keyboard::PhysicalKey::Code(winit::keyboard::KeyCode::F11) => {
                    if event.state.is_pressed() {
                        let monitor = self.window.current_monitor().unwrap();
                        self.window.set_fullscreen(if self.window.fullscreen().is_none() {
                            Some(winit::window::Fullscreen::Borderless(Some(monitor)))
                        } else {
                            None
                        });
                    }
                    true
                }
                _ => self.camera_controller.process_keyboard(&event.physical_key, event.state),
            },
            winit::event::WindowEvent::MouseWheel { delta, .. } => {
                self.camera_controller.process_scroll(delta);
                true
            }
            winit::event::WindowEvent::MouseInput {
                state,
                button: MouseButton::Left,
                ..
            } => {
                self.state.mouse_pressed = *state == ElementState::Pressed;
                true
            }
            _ => false,
        }
    }
    pub(crate) fn destroy(self) {
        unsafe {
            self.device.unmap_buffer(&self.camera_buffer);
            self.device.unmap_buffer(&self.rng_buffer);
            self.device.unmap_buffer(&self.custom_buffer);
            self.device.destroy_command_encoder(self.encoder);

            self.gui_infra.destroy(&self.device);
            self.post_process_infra.destroy(&self.device);
            self.helper_textures.destroy(&self.device);
            self.device.destroy_sampler(self.world_sampler);

            for i2 in self.gltf_scenes {
                self.device.destroy_buffer(i2.blas.scratch);
                self.device.destroy_acceleration_structure(i2.blas.blas);
                for u in i2.materials {
                    u.destroy(&self.device)
                }
            }
            for i in self.other_models.models {
                self.device.destroy_buffer(i.blas.scratch);
                self.device.destroy_acceleration_structure(i.blas.blas);
                for u in i.materials {
                    u.destroy(&self.device)
                }
            }
            self.device.destroy_compute_pipeline(self.render_pipeline);
            self.device.destroy_pipeline_layout(self.rp_layout);

            self.device.destroy_bind_group(self.main_bind_group);
            self.device.destroy_bind_group(self.rt_bind_group);
            self.device.destroy_bind_group(self.camera_bind_group);

            self.device.destroy_bind_group_layout(self.rt_bgl);
            self.device.destroy_bind_group_layout(self.bgl);
            self.device.destroy_bind_group_layout(self.camera_bgl);

            self.device.destroy_texture_view(self.dest_texture_view);
            self.device.destroy_texture(self.dest_texture);

            self.device.destroy_buffer(self.pf_camera_uniform);
            self.device.destroy_buffer(self.rng_buffer);
            self.device.destroy_buffer(self.camera_buffer);
            self.device.destroy_buffer(self.custom_buffer);

            self.device.destroy_acceleration_structure(self.tlas.tlas);
            self.device.destroy_buffer(self.tlas.scratch);
            self.device.destroy_buffer(self.tlas.instances_buffer);

            self.device.destroy_shader_module(self.frag_shader);
            drop(self.filtering_infra);

            self.surface.unconfigure(&self.device);
            let dev = Rc::try_unwrap(self.device).ok().unwrap();
            dev.exit(self.queue);
            self.instance.destroy_surface(self.surface);

            drop(self.adapter);
            drop(self.instance);
        }
    }
    pub fn render(&mut self, fence: &mut wgpu_hal::vulkan::Fence, fence_value: FenceValue, last_render_time: &mut std::time::Instant) {
        unsafe {
            let now = std::time::Instant::now();
            let dt = now - *last_render_time;
            *last_render_time = now;
            self.encoder.begin_encoding(Some("EncodingSession")).unwrap();
            let current_texture = self.surface.acquire_texture(None, fence).unwrap().unwrap().texture;
            self.encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: current_texture.borrow(),
                range: wgpu_types::ImageSubresourceRange::default(),
                usage: wgpu_hal::TextureUses::UNINITIALIZED..wgpu_hal::TextureUses::COLOR_TARGET,
            }));

            self.post_process_infra.write_to_resource(&mut self.encoder); // pf tex can be read

            self.helper_textures.pf_src_to_storage(&mut self.encoder); //pf can be written
            self.helper_textures.read_to_write(&mut self.encoder); // g buffers can be written

            let current_view = self
                .device
                .create_texture_view(
                    current_texture.borrow(),
                    &wgpu_hal::TextureViewDescriptor {
                        label: Some("FrameView"),
                        format: self.surface_config.format,
                        dimension: wgt::TextureViewDimension::D2,
                        usage: wgpu_hal::TextureUses::COLOR_TARGET | wgpu_hal::TextureUses::COPY_DST,
                        range: wgt::ImageSubresourceRange::default(),
                    },
                )
                .unwrap();
            for i2 in self.gltf_scenes.iter_mut() {
                i2.update_skinned_model();
                i2.update_blas(&self.device, &mut self.encoder);
            }
            self.update_tlas();
            self.encoder.begin_compute_pass(&ComputePassDescriptor {
                label: None,
                timestamp_writes: None,
            });
            self.encoder.set_compute_pipeline(&self.render_pipeline);
            self.encoder.set_bind_group(&self.rp_layout, 0, &self.main_bind_group, &[]);
            self.encoder.set_bind_group(&self.rp_layout, 1, &self.camera_bind_group, &[]);
            self.encoder.set_bind_group(&self.rp_layout, 2, &self.rt_bind_group, &[]);
            self.encoder
                .set_bind_group(&self.rp_layout, 3, &self.helper_textures.bind_group, &[]);
            self.encoder.dispatch([
                self.surface_config.extent.width / 32 + 1,
                self.surface_config.extent.height / 32 + 1,
                1,
            ]);
            self.camera_controller.update_camera(&mut self.camera, dt);
            self.camera_uniform.update_view_proj(&self.camera, &self.projection);
            self.encoder.end_compute_pass();
            self.helper_textures.write_to_read(&mut self.encoder); //g buffers can be read
            self.helper_textures.read_to_write(&mut self.encoder);
            // self.helper_textures.pf_storage_to_r(&mut self.encoder);

            self.encoder.begin_compute_pass(&ComputePassDescriptor {
                label: None,
                timestamp_writes: None,
            });
            self.encoder.set_compute_pipeline(self.filtering_infra.pipeline());
            self.encoder.set_bind_group(
                self.filtering_infra.pipeline_layout(),
                0,
                &self.post_process_infra.bind_group(),
                &[],
            );
            self.encoder
                .set_bind_group(self.filtering_infra.pipeline_layout(), 1, &self.helper_textures.bind_group, &[]);
            self.encoder
                .set_bind_group(self.filtering_infra.pipeline_layout(), 2, &self.camera_bind_group, &[]);
            self.encoder.dispatch([
                self.surface_config.extent.width / 32 + 1,
                self.surface_config.extent.height / 32 + 1,
                1,
            ]);
            self.encoder.end_compute_pass();
            self.helper_textures.write_to_read(&mut self.encoder);

            self.gui_infra.render(&mut self.encoder);

            self.encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &self.dest_texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE..wgpu_hal::TextureUses::RESOURCE,
            }));
            self.encoder.begin_render_pass(&wgpu_hal::RenderPassDescriptor {
                label: None,
                extent: Extent3d {
                    width: self.surface_config.extent.width,
                    height: self.surface_config.extent.height,
                    depth_or_array_layers: 1,
                },
                sample_count: 1,
                color_attachments: &[Some(wgpu_hal::ColorAttachment {
                    target: wgpu_hal::Attachment {
                        view: &current_view,
                        usage: wgpu_hal::TextureUses::COLOR_TARGET,
                    },
                    resolve_target: None,
                    ops: wgpu_hal::AttachmentOps::STORE,
                    clear_value: Default::default(),
                })],
                depth_stencil_attachment: None,
                multiview: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });
            self.encoder.set_render_pipeline(&self.post_process_infra.pipeline);
            self.encoder.set_bind_group(
                &self.post_process_infra.pipeline_layout,
                0,
                self.post_process_infra.bind_group.as_ref().unwrap(),
                &[],
            );
            self.encoder
                .set_bind_group(&self.post_process_infra.pipeline_layout, 1, &self.helper_textures.bind_group, &[]);
            self.encoder
                .set_bind_group(&self.post_process_infra.pipeline_layout, 2, &self.camera_bind_group, &[]);
            self.encoder.draw(0, 3, 0, 1);
            self.encoder.end_render_pass();

            // self.helper_textures.pf_storage_to_rw(&mut self.encoder);
            self.helper_textures.pf_storage_to_src(&mut self.encoder);
            self.post_process_infra.resource_to_write(&mut self.encoder);
            self.post_process_infra.copy_from_pf(&mut self.encoder, &self.helper_textures);

            self.encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
                buffer: &self.camera_buffer,
                usage: BufferUses::UNIFORM..BufferUses::COPY_SRC,
            }));
            self.encoder.copy_buffer_to_buffer(
                &self.camera_buffer,
                &self.pf_camera_uniform,
                once(wgpu_hal::BufferCopy {
                    src_offset: 0,
                    dst_offset: 0,
                    size: CameraUniform::SIZE,
                }),
            );

            self.encoder.transition_buffers(once(wgpu_hal::BufferBarrier {
                buffer: &self.camera_buffer,
                usage: BufferUses::COPY_SRC..BufferUses::UNIFORM,
            }));
            self.encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: current_texture.borrow(),
                range: Default::default(),
                usage: wgpu_hal::TextureUses::COLOR_TARGET..wgpu_hal::TextureUses::PRESENT,
            }));
            self.encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &self.dest_texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::RESOURCE..wgpu_hal::TextureUses::STORAGE_READ_WRITE,
            }));

            let command_buffer = self.encoder.end_encoding().unwrap();
            self.queue
                .submit(&[&command_buffer], &[&current_texture], (fence, fence_value))
                .unwrap();
            self.queue.present(&self.surface, current_texture).unwrap();
            self.device.wait(fence, fence_value, !0).unwrap();
            self.encoder.reset_all(once(command_buffer));
            self.device.destroy_texture_view(current_view);
        }
    }
    pub fn resize(&mut self, u: PhysicalSize<u32>) {
        unsafe {
            self.surface.unconfigure(&self.device);
            let surface = self
                .instance
                .create_surface(
                    self.window.display_handle().unwrap().as_raw(),
                    self.window.window_handle().unwrap().as_raw(),
                )
                .unwrap();
            let capabilities = self.adapter.adapter.surface_capabilities(&surface).unwrap();
            let capable_resolution = Extent3d {
                width: capabilities.current_extent.unwrap().width,
                height: capabilities.current_extent.unwrap().height,
                depth_or_array_layers: 1,
            };
            self.projection.resize(capable_resolution.width, capable_resolution.height);
            self.surface_config.extent = capable_resolution;
            surface.configure(&self.device, &self.surface_config).unwrap();
            let dest_texture = self
                .device
                .create_texture(&wgpu_hal::TextureDescriptor {
                    label: None,
                    size: capable_resolution,
                    mip_level_count: 1,
                    sample_count: 1,
                    dimension: wgt::TextureDimension::D2,
                    format: self.surface_config.format,
                    usage: wgpu_hal::TextureUses::COPY_SRC | wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::RESOURCE,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                    view_formats: vec![],
                })
                .unwrap();
            let dest_texture_view = self
                .device
                .create_texture_view(
                    &dest_texture,
                    &wgpu_hal::TextureViewDescriptor {
                        label: None,
                        format: self.surface_config.format,
                        dimension: wgt::TextureViewDimension::D2,
                        usage: wgpu_hal::TextureUses::COPY_SRC
                            | wgpu_hal::TextureUses::STORAGE_READ_WRITE
                            | wgpu_hal::TextureUses::RESOURCE,
                        range: Default::default(),
                    },
                )
                .unwrap();
            self.gui_infra.resize(u, &self.device, &mut self.queue, &mut self.encoder);
            self.encoder.begin_encoding(None).unwrap();
            self.encoder.transition_textures(once(wgpu_hal::TextureBarrier {
                texture: &dest_texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::UNINITIALIZED..wgpu_hal::TextureUses::STORAGE_READ_WRITE,
            }));
            submit_and_wait(&mut self.encoder, &self.device, &self.queue).unwrap();
            let old_texture = replace(&mut self.dest_texture, dest_texture);
            let old_view = replace(&mut self.dest_texture_view, dest_texture_view);
            self.device.destroy_texture_view(old_view);
            self.device.destroy_texture(old_texture);
            let rt_bind_group = self
                .device
                .create_bind_group(&wgpu_hal::BindGroupDescriptor {
                    label: Some("RT bind group"),
                    layout: &self.rt_bgl,
                    buffers: &[],
                    samplers: &[],
                    textures: &[wgpu_hal::TextureBinding {
                        view: &self.dest_texture_view,
                        usage: wgpu_hal::TextureUses::COPY_SRC | wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    }],
                    entries: &[
                        wgpu_hal::BindGroupEntry {
                            binding: 0,
                            resource_index: 0,
                            count: 1,
                        },
                        wgpu_hal::BindGroupEntry {
                            binding: 1,
                            resource_index: 0,
                            count: 1,
                        },
                    ],
                    acceleration_structures: &[&self.tlas.tlas],
                })
                .unwrap();
            let old_rt_bg = replace(&mut self.rt_bind_group, rt_bind_group);
            let old_surface = replace(&mut self.surface, surface);
            self.instance.destroy_surface(old_surface);
            self.device.destroy_bind_group(old_rt_bg);

            self.helper_textures
                .resize(&self.device, &mut self.queue, &mut self.encoder, capable_resolution);

            self.post_process_infra.resize(
                &mut self.device,
                &mut self.queue,
                &mut self.encoder,
                &self.dest_texture_view,
                &self.gui_infra.dest_texture.view,
                capable_resolution,
            );
            self.filtering_infra
                .resize(&self.post_process_infra, &self.helper_textures, &self.camera_bgl)
        }
    }
    pub fn update_tlas(&mut self) {
        unsafe {
            write_buffer(
                cast_slice(self.tlas.instances.as_slice()),
                &self.device,
                &self.tlas.instances_buffer,
            );
            self.encoder.build_acceleration_structures(
                1,
                once(wgpu_hal::BuildAccelerationStructureDescriptor {
                    entries: &wgpu_hal::AccelerationStructureEntries::Instances(wgpu_hal::AccelerationStructureInstances {
                        buffer: Some(&self.tlas.instances_buffer),
                        offset: 0,
                        count: self.tlas.instances.len() as u32,
                    }),
                    mode: wgpu_hal::AccelerationStructureBuildMode::Update,
                    flags: wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE
                        | wgpu_hal::AccelerationStructureBuildFlags::PREFER_FAST_TRACE
                        | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_COMPACTION,
                    source_acceleration_structure: None,
                    destination_acceleration_structure: &self.tlas.tlas,
                    scratch_buffer: &self.tlas.scratch,
                    scratch_buffer_offset: 0,
                }),
            )
        }
    }
    pub fn update_state(&mut self) {
        let mut raw_input = self.egui_state.take_egui_input(&self.window);
        raw_input
            .viewports
            .insert(self.egui_state.egui_ctx().viewport_id(), Default::default())
            .unwrap();
        let mut old_transform = self.tlas.instances[self.state.selected_index as usize]
            .transform_as_affine()
            .to_scale_rotation_translation();
        let mut old_rotation = old_transform.1.to_euler(glam::EulerRot::XYZ);
        let old_bone = self.gltf_scenes[0].skeleton.as_ref().unwrap()[self.state.skeletal_bone];
        let old_bone_transform = old_bone.transform_matrix.to_scale_rotation_translation();
        let mut old_bone_rotation = old_bone_transform.1.to_euler(glam::EulerRot::XYZ);
        let mut next_selected_bone = self.state.skeletal_bone;
        for i in raw_input.events.iter() {
            if let egui::Event::PointerMoved(u) = i {
                self.state.custom_parameter[1..3].copy_from_slice(&[u.x, u.y])
            }
        }
        let gui_output = self.egui_state.egui_ctx().run(raw_input.clone(), |ctx| {
            egui::Window::new("GUI").show(ctx, |ui| {
                ui.label("Hello world!");
                ui.button("Click me").clicked();
                ui.label("Parameter 1");
                ui.add(egui::Slider::new(&mut self.state.custom_parameter[0], 0.0..=1.0));
                ui.label("TRANSLATION");
                ui.horizontal(|x| {
                    x.add(egui::DragValue::new(&mut old_transform.2.x).speed(0.01).fixed_decimals(2));
                    x.add(egui::DragValue::new(&mut old_transform.2.y).speed(0.01).fixed_decimals(2));
                    x.add(egui::DragValue::new(&mut old_transform.2.z).speed(0.01).fixed_decimals(2));
                });
                ui.label("ROTATION");
                ui.horizontal(|x| {
                    x.add(egui::DragValue::new(&mut old_rotation.0).speed(0.01).fixed_decimals(2));
                    x.add(egui::DragValue::new(&mut old_rotation.1).speed(0.01).fixed_decimals(2));
                    x.add(egui::DragValue::new(&mut old_rotation.2).speed(0.01).fixed_decimals(2));
                });
                ui.label("Selected object");
                ui.code(format!("{}", self.state.selected_index));
                ui.label("Bone Translation");
                ui.horizontal(|x| {
                    x.add(egui::DragValue::new(&mut old_bone_rotation.0).speed(0.01).fixed_decimals(2));
                    x.add(egui::DragValue::new(&mut old_bone_rotation.1).speed(0.01).fixed_decimals(2));
                    x.add(egui::DragValue::new(&mut old_bone_rotation.2).speed(0.01).fixed_decimals(2));
                });
                ui.label("Selected bone");
                ui.add(egui::DragValue::new(&mut next_selected_bone).speed(1).range(0..=1));
                ui.label("Debug layer");
                egui::ComboBox::from_label("")
                    .selected_text(format!("{:?}", self.state.custom_parameter[4]))
                    .show_ui(ui, |y| {
                        y.selectable_value(&mut self.state.custom_parameter[4], 0.0, "No debug");
                        y.selectable_value(&mut self.state.custom_parameter[4], 1.0, "Albedo");
                        y.selectable_value(&mut self.state.custom_parameter[4], 2.0, "Normal");
                        y.selectable_value(&mut self.state.custom_parameter[4], 3.0, "Position");
                        y.selectable_value(&mut self.state.custom_parameter[4], 4.0, "Direct Light");
                        y.selectable_value(&mut self.state.custom_parameter[4], 5.0, "Depth");
                        y.selectable_value(&mut self.state.custom_parameter[4], 6.0, "Raw color");
                        y.selectable_value(&mut self.state.custom_parameter[4], 7.0, "Indirect light");
                    });

                self.state.interacting_with_ui = ui.rect_contains_pointer(egui::Rect {
                    min: egui::Pos2::ZERO,
                    max: egui::Pos2::new(590.0, 290.0),
                });
            });
        });
        self.gltf_scenes[0].skeleton.as_mut().unwrap()[self.state.skeletal_bone].transform_matrix = Mat4::from_scale_rotation_translation(
            old_bone_transform.0,
            Quat::from_euler(glam::EulerRot::XYZ, old_bone_rotation.0, old_bone_rotation.1, old_bone_rotation.2),
            old_bone_transform.2,
        );
        self.tlas.instances[self.state.selected_index as usize].set_transform(&Affine3A::from_scale_rotation_translation(
            old_transform.0,
            Quat::from_euler(glam::EulerRot::XYZ, old_rotation.0, old_rotation.1, old_rotation.2),
            old_transform.2,
        ));
        self.state.skeletal_bone = next_selected_bone;
        let egui_meshes = self.egui_state.egui_ctx().tessellate(gui_output.shapes, 1.0);
        self.gui_infra.update(
            &self.device,
            gui_output.textures_delta,
            egui_meshes,
            &mut self.queue,
            &mut self.encoder,
        );
    }
}
impl Default for AppState {
    fn default() -> Self {
        Self {
            custom_parameter: [0.0f32; 16],
            mouse_pressed: false,
            selected_index: 0,
            return_from_shader: [0u8; 64],
            interacting_with_ui: false,
            skeletal_bone: 0,
            is_resize_needed: None,
        }
    }
}

#[inline]
pub fn joint_matrix(bone: Bone) -> Mat4 {
    bone.transform_matrix * bone.inv_bind_matrix
}
