use std::rc::Rc;

use wgpu_hal::vulkan::{ComputePipeline, PipelineLayout};
use wgpu_hal::{ComputePipelineDescriptor, Device, ShaderInput, ShaderModuleDescriptor};

use crate::g_buffers::HelperTextures;
use crate::post_processing::PostProcessing;
use crate::resources::make_spirv_raw;

pub struct Filtering {
    shader: Option<wgpu_hal::vulkan::ShaderModule>,
    pipeline_layout: Option<PipelineLayout>,
    render_pipeline: Option<ComputePipeline>,
    device: Rc<wgpu_hal::vulkan::Device>,
}
impl Filtering {
    pub fn new(
        device: &Rc<wgpu_hal::vulkan::Device>,
        post_processing: &PostProcessing,
        helper_textures: &HelperTextures,
        camera_bgl: &wgpu_hal::vulkan::BindGroupLayout,
    ) -> Filtering {
        let shader = unsafe {
            device
                .create_shader_module(
                    &ShaderModuleDescriptor {
                        label: None,
                        runtime_checks: false,
                    },
                    ShaderInput::SpirV(&make_spirv_raw(include_bytes!("../shaders/filtering.spv"))),
                )
                .unwrap()
        };
        let mut result = Self {
            shader: Some(shader),
            pipeline_layout: None,
            render_pipeline: None,
            device: device.clone(),
        };
        result.resize(post_processing, helper_textures, camera_bgl);
        // let render_pipeline_layout = unsafe {
        //     device
        //         .create_pipeline_layout(&wgpu_hal::PipelineLayoutDescriptor {
        //             label: None,
        //             flags: wgpu_hal::PipelineLayoutFlags::empty(),
        //             bind_group_layouts: &[&post_processing.bind_group_layout, &helper_textures.bgl, camera_bgl],
        //             push_constant_ranges: &[],
        //         })
        //         .unwrap()
        // };
        // let render_pipeline = unsafe {
        //     device
        //         .create_compute_pipeline(&ComputePipelineDescriptor {
        //             label: None,
        //             layout: &render_pipeline_layout,
        //             stage: wgpu_hal::ProgrammableStage {
        //                 module: &shader,
        //                 entry_point: "main",
        //                 constants: &Default::default(),
        //                 zero_initialize_workgroup_memory: false,
        //             },
        //         })
        //         .unwrap()

        result
    }
    pub fn resize(
        &mut self,
        post_processing: &PostProcessing,
        helper_textures: &HelperTextures,
        camera_bgl: &wgpu_hal::vulkan::BindGroupLayout,
    ) {
        let new_ppl_l = unsafe {
            self.device
                .create_pipeline_layout(&wgpu_hal::PipelineLayoutDescriptor {
                    label: None,
                    flags: wgpu_hal::PipelineLayoutFlags::empty(),
                    bind_group_layouts: &[&post_processing.bind_group_layout, &helper_textures.bgl, camera_bgl],
                    push_constant_ranges: &[],
                })
                .unwrap()
        };
        let new_ppl = unsafe {
            self.device
                .create_compute_pipeline(&ComputePipelineDescriptor {
                    label: None,
                    layout: &new_ppl_l,
                    stage: wgpu_hal::ProgrammableStage {
                        module: &self.shader.as_ref().unwrap(),
                        entry_point: "main",
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                        vertex_pulling_transform: false,
                    },
                    cache: None,
                })
                .unwrap()
        };
        let old_ppl_l = self.pipeline_layout.replace(new_ppl_l);
        let old_ppl = self.render_pipeline.replace(new_ppl);
        if let Some(i) = old_ppl {
            unsafe { self.device.destroy_compute_pipeline(i) }
        }
        if let Some(i) = old_ppl_l {
            unsafe { self.device.destroy_pipeline_layout(i) }
        }
    }
    #[inline]
    pub fn pipeline_layout(&self) -> &PipelineLayout {
        self.pipeline_layout.as_ref().unwrap()
    }
    #[inline]
    pub fn pipeline(&self) -> &ComputePipeline {
        self.render_pipeline.as_ref().unwrap()
    }
}

impl Drop for Filtering {
    fn drop(&mut self) {
        unsafe {
            let dev: &wgpu_hal::vulkan::Device = self.device.as_ref();
            dev.destroy_compute_pipeline(self.render_pipeline.take().unwrap());
            dev.destroy_pipeline_layout(self.pipeline_layout.take().unwrap());
            dev.destroy_shader_module(self.shader.take().unwrap());
        }
    }
}
