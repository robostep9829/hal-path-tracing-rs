use std::io::Cursor;
use std::mem::replace;

use anyhow::*;
use image::GenericImageView;
use image::ImageDecoder;
use wgpu_hal::{CommandEncoder, Device};
use wgpu_types::Extent3d;

use crate::submit_and_wait;

pub struct Texture {
    pub texture: wgpu_hal::vulkan::Texture,
    pub view: wgpu_hal::vulkan::TextureView,
    pub texture_buffer: wgpu_hal::vulkan::Buffer,
    pub size: Extent3d,
    pub format: wgpu_types::TextureFormat,
    pub uses: wgpu_hal::TextureUses,
}

impl Texture {
    pub unsafe fn from_bytes(
        device: &wgpu_hal::vulkan::Device,
        queue: &mut wgpu_hal::vulkan::Queue,
        bytes: &[u8],
        label: &str,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    ) -> Result<Self> {
        let mut reader = image::ImageReader::new(Cursor::new(bytes)).with_guessed_format().unwrap();
        if let None = reader.format() {
            if label.contains("tga") {
                reader.set_format(image::ImageFormat::Tga)
            } else {
                panic!("Unable to determine image format")
            }
        }
        let img = reader.decode().unwrap();
        Self::from_image(device, queue, &img, Some(label), command_encoder)
    }

    fn from_image(
        device: &wgpu_hal::vulkan::Device,
        queue: &mut wgpu_hal::vulkan::Queue,
        img: &image::DynamicImage,
        _label: Option<&str>,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    ) -> Result<Self> {
        let rgba = img.to_rgba8();
        let dimensions = img.dimensions();
        let size = Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth_or_array_layers: 1,
        };
        let text = Self::from_size(
            device,
            queue,
            (size.width, size.height),
            None,
            command_encoder,
            None,
            wgpu_types::TextureFormat::Rgba8UnormSrgb,
        )
        .unwrap();
        super::write_buffer(&rgba, device, &text.texture_buffer);
        unsafe {
            command_encoder.begin_encoding(None).unwrap();
            command_encoder.transition_buffers(std::iter::once(wgpu_hal::BufferBarrier {
                buffer: &text.texture_buffer,
                usage: wgpu_hal::BufferUses::empty()..wgpu_hal::BufferUses::COPY_SRC,
            }));
            command_encoder.copy_buffer_to_texture(
                &text.texture_buffer,
                &text.texture,
                std::iter::once(wgpu_hal::BufferTextureCopy {
                    buffer_layout: wgpu_types::ImageDataLayout {
                        offset: 0,
                        bytes_per_row: Some(4 * size.width),
                        rows_per_image: Some(size.height),
                    },
                    texture_base: wgpu_hal::TextureCopyBase {
                        mip_level: 0,
                        array_layer: 0,
                        origin: wgpu_types::Origin3d::ZERO,
                        aspect: wgpu_hal::FormatAspects::COLOR,
                    },
                    size: wgpu_hal::CopyExtent {
                        width: size.width,
                        height: size.height,
                        depth: size.depth_or_array_layers,
                    },
                }),
            );
            command_encoder.transition_textures(std::iter::once(wgpu_hal::TextureBarrier {
                texture: &text.texture,
                range: Default::default(),
                usage: wgpu_hal::TextureUses::COPY_DST..wgpu_hal::TextureUses::RESOURCE,
            }));
            command_encoder.transition_buffers(std::iter::once(wgpu_hal::BufferBarrier {
                buffer: &text.texture_buffer,
                usage: wgpu_hal::BufferUses::COPY_SRC..wgpu_hal::BufferUses::COPY_DST,
            }));
            command_encoder.clear_buffer(
                &text.texture_buffer,
                0..(size.width * size.height * size.depth_or_array_layers * 4) as u64,
            );
            submit_and_wait(command_encoder, device, queue).expect("TODO: panic message");
        };
        Ok(text)
    }

    ///Use when not recording
    pub fn from_size(
        device: &wgpu_hal::vulkan::Device,
        queue: &mut wgpu_hal::vulkan::Queue,
        size: (u32, u32),
        label: Option<&str>,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        usages: Option<wgpu_hal::TextureUses>,
        format: wgpu_types::TextureFormat,
    ) -> Result<Self> {
        unsafe {
            let size = Extent3d {
                width: size.0.max(1),
                height: size.1.max(1),
                depth_or_array_layers: 1,
            };
            let texture = device
                .create_texture(&wgpu_hal::TextureDescriptor {
                    label,
                    size,
                    mip_level_count: 1,
                    sample_count: 1,
                    dimension: wgpu_types::TextureDimension::D2,
                    format,
                    usage: wgpu_hal::TextureUses::COPY_DST | usages.unwrap_or(wgpu_hal::TextureUses::RESOURCE),
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                    view_formats: vec![],
                })
                .unwrap();
            let texture_buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label,
                    size: (size.width * size.height * size.depth_or_array_layers * 4) as u64,
                    usage: wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::COPY_SRC | wgpu_hal::BufferUses::COPY_DST,
                    memory_flags: wgpu_hal::MemoryFlags::TRANSIENT | wgpu_hal::MemoryFlags::PREFER_COHERENT,
                })
                .unwrap();
            let view = device
                .create_texture_view(
                    &texture,
                    &wgpu_hal::TextureViewDescriptor {
                        label,
                        format,
                        dimension: wgpu_types::TextureViewDimension::D2,
                        usage: wgpu_hal::TextureUses::COPY_DST | usages.unwrap_or(wgpu_hal::TextureUses::RESOURCE),
                        range: wgpu_types::ImageSubresourceRange::default(),
                    },
                )
                .unwrap();
            {
                command_encoder.begin_encoding(None).unwrap();
                command_encoder.transition_buffers(std::iter::once(wgpu_hal::BufferBarrier {
                    buffer: &texture_buffer,
                    usage: wgpu_hal::BufferUses::empty()..wgpu_hal::BufferUses::COPY_SRC,
                }));
                command_encoder.transition_textures(std::iter::once(wgpu_hal::TextureBarrier {
                    texture: &texture,
                    range: Default::default(),
                    usage: wgpu_hal::TextureUses::UNINITIALIZED..usages.unwrap_or(wgpu_hal::TextureUses::COPY_DST),
                }));
                submit_and_wait(command_encoder, device, queue).expect("TODO: panic message");
            }
            Ok(Self {
                texture,
                view,
                texture_buffer,
                size: Extent3d {
                    width: size.width,
                    height: size.height,
                    depth_or_array_layers: 1,
                },
                format,
                uses: usages.unwrap_or(wgpu_hal::TextureUses::RESOURCE),
            })
        }
    }
    pub const _DEPTH_FORMAT: wgpu_types::TextureFormat = wgpu_types::TextureFormat::Depth32Float;

    pub unsafe fn _create_depth_texture(device: &wgpu_hal::vulkan::Device, size: Extent3d, label: &str) -> Self {
        let texture = device
            .create_texture(&wgpu_hal::TextureDescriptor {
                label: Some(label),
                size,
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu_types::TextureDimension::D2,
                format: Self::_DEPTH_FORMAT,
                usage: wgpu_hal::TextureUses::DEPTH_STENCIL_WRITE,
                memory_flags: wgpu_hal::MemoryFlags::empty(),
                view_formats: vec![],
            })
            .unwrap();
        let view = device
            .create_texture_view(
                &texture,
                &wgpu_hal::TextureViewDescriptor {
                    label: None,
                    format: Self::_DEPTH_FORMAT,
                    dimension: wgpu_types::TextureViewDimension::D2,
                    usage: wgpu_hal::TextureUses::DEPTH_STENCIL_WRITE,
                    range: Default::default(),
                },
            )
            .unwrap();
        let _sampler = device
            .create_sampler(&wgpu_hal::SamplerDescriptor {
                label: None,
                address_modes: [
                    wgpu_types::AddressMode::ClampToEdge,
                    wgpu_types::AddressMode::ClampToEdge,
                    wgpu_types::AddressMode::ClampToEdge,
                ],
                mag_filter: Default::default(),
                min_filter: Default::default(),
                mipmap_filter: Default::default(),
                lod_clamp: Default::default(),
                compare: Some(wgpu_types::CompareFunction::LessEqual),
                anisotropy_clamp: 1,
                border_color: None,
            })
            .unwrap();
        let buffer = device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: None,
                size: 1,
                usage: wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::COPY_SRC,
                memory_flags: wgpu_hal::MemoryFlags::empty(),
            })
            .unwrap();

        Self {
            texture,
            view,
            texture_buffer: buffer,
            size: Extent3d {
                width: size.width,
                height: size.height,
                depth_or_array_layers: 1,
            },
            format: wgpu_types::TextureFormat::Depth32Float,
            uses: wgpu_hal::TextureUses::DEPTH_STENCIL_WRITE,
        }
    }
    pub fn destroy(self, device: &wgpu_hal::vulkan::Device) {
        unsafe {
            // device.destroy_sampler(self.sampler);
            device.destroy_texture_view(self.view);
            device.destroy_texture(self.texture);
            device.destroy_buffer(self.texture_buffer)
        }
    }
    pub fn resize(
        &mut self,
        device: &wgpu_hal::vulkan::Device,
        queue: &mut wgpu_hal::vulkan::Queue,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        size: Extent3d,
    ) {
        let new_texture = Texture::from_size(
            device,
            queue,
            (size.width, size.height),
            None,
            command_encoder,
            Some(self.uses),
            self.format,
        )
        .unwrap();
        let old_texture = replace(self, new_texture);
        old_texture.destroy(device);
    }
    pub fn create_sampler(device: &wgpu_hal::vulkan::Device) -> wgpu_hal::vulkan::Sampler {
        unsafe {
            device.create_sampler(&wgpu_hal::SamplerDescriptor {
                label: Some("Diffuse Sampler"),
                address_modes: [
                    wgpu_types::AddressMode::ClampToEdge,
                    wgpu_types::AddressMode::ClampToEdge,
                    wgpu_types::AddressMode::ClampToEdge,
                ],
                mag_filter: wgpu_types::FilterMode::Linear,
                min_filter: wgpu_types::FilterMode::Nearest,
                mipmap_filter: wgpu_types::FilterMode::Nearest,
                lod_clamp: Default::default(),
                compare: None,
                anisotropy_clamp: 2,
                border_color: None,
            })
        }
        .unwrap()
    }
}
