use std::io::{BufReader, Cursor};
use std::mem::{replace, size_of, size_of_val};

use bytemuck::cast_slice;
use egui::TextBuffer;
use glam::{Affine3A, Mat4, Vec3};
use rayon::prelude::*;
use wgpu_hal::{AccelerationStructureTriangles, CommandEncoder, Device};

use crate::accel_struct::AccelerationStructureInstance;
use crate::model::{Bone, GltfModel, Material, Model, ModelVertex};
use crate::texture::Texture;
use crate::{submit_and_wait, write_buffer, write_texture, BlasContainer, ObjInstance, TlasContainer};

const MAX_U24: u32 = (1u32 << 24u32) - 1u32;
pub struct ModelArena {
    pub models: Vec<Model>,
    rt_index_offset: u32,
}
impl ModelArena {
    fn push(&mut self, model: Model, material_count: u32) {
        self.models.push(model);
        self.rt_index_offset += material_count;
        debug_assert!(
            self.rt_index_offset <= MAX_U24,
            "shader_binding_table_record_offset uses more than 24 bits! X > {}",
            MAX_U24
        );
    }
    pub fn rt_index_offset(&self) -> u32 {
        self.rt_index_offset
    }
    pub fn new() -> Self {
        Self {
            models: vec![],
            rt_index_offset: 0,
        }
    }
}
pub async fn load_model(
    file_name: &str,
    device: &wgpu_hal::vulkan::Device,
    queue: &mut wgpu_hal::vulkan::Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    instances_orig: &[ObjInstance],
    tlas_app: &mut TlasContainer,
    model_arena: &mut ModelArena,
) -> anyhow::Result<()> {
    let obj_text = load_string(file_name).await?;
    let obj_cursor = Cursor::new(obj_text);
    let mut obj_reader = BufReader::new(obj_cursor);
    let split_path: Vec<_> = file_name.split("/").collect();
    // let relative_path = split_path.split_last().unwrap().1;
    let relative_path = if let Some(x) = split_path.split_last() {
        if !x.1.is_empty() {
            x.1.join("/") + "/"
        } else {
            "".to_string()
        }
    } else {
        "".to_string()
    };

    let (models, obj_materials) = tobj::load_obj_buf_async(
        &mut obj_reader,
        &tobj::LoadOptions {
            triangulate: true,
            single_index: true,
            ..Default::default()
        },
        |p| {
            let value = relative_path.clone();
            async move {
                let mat_text = load_string((value + &p).as_str()).await.unwrap();
                tobj::load_mtl_buf(&mut BufReader::new(Cursor::new(mat_text)))
            }
        },
    )
    .await?;
    let mut materials = Vec::new();
    for (u, m) in obj_materials?.iter().enumerate() {
        let diffuse_texture = if let Some(texture_name) = &m.diffuse_texture {
            load_texture((relative_path.clone() + texture_name).as_str(), device, queue, command_encoder).await?
        } else {
            let tex = Texture::from_size(
                device,
                queue,
                (1, 1),
                None,
                command_encoder,
                None,
                wgpu_types::TextureFormat::Rgba8UnormSrgb,
            )
            .unwrap();
            if let Some(v_color) = models[0].mesh.vertex_color.get(0..4) {
                write_texture(
                    cast_slice(v_color),
                    device,
                    &tex,
                    wgpu_types::Origin2d::ZERO,
                    command_encoder,
                    queue,
                )
            } else {
                write_texture(
                    cast_slice(&[128u8, 128, 128, 255]),
                    device,
                    &tex,
                    wgpu_types::Origin2d::ZERO,
                    command_encoder,
                    queue,
                )
            }
            tex
        };
        let current_models = models.iter().filter(|x| x.mesh.material_id.unwrap_or(0) == u);
        let mut all_vertices = vec![];
        for m in current_models.clone() {
            let vertices = (0..m.mesh.positions.len() / 3)
                .map(|i| ModelVertex {
                    position: [m.mesh.positions[i * 3], m.mesh.positions[i * 3 + 1], m.mesh.positions[i * 3 + 2]],
                    tex_coords: [m.mesh.texcoords[i * 2], m.mesh.texcoords[i * 2 + 1]],
                    normal: [m.mesh.normals[i * 3], m.mesh.normals[i * 3 + 1], m.mesh.normals[i * 3 + 2]],
                    joints: [0, 0, 0, 0],
                    weights: [0.0, 0.0, 0.0, 0.0],
                })
                .collect::<Vec<_>>();
            for i in vertices {
                all_vertices.push(i)
            }
        }
        let mut all_indices = vec![];
        {
            let mut length_sf = 0;
            for i in current_models {
                for a in &i.mesh.indices {
                    all_indices.push(a + length_sf)
                }
                length_sf += i.mesh.indices.len() as u32;
            }
        }
        let vertex_buffer = {
            let content = cast_slice(&all_vertices);
            let size = size_of_val(content);
            let buffer = unsafe {
                device
                    .create_buffer(&wgpu_hal::BufferDescriptor {
                        label: Some(&format!("{:?} Vertex Buffer", file_name)),
                        size: size as u64,
                        usage: wgpu_hal::BufferUses::VERTEX
                            | wgpu_hal::BufferUses::MAP_WRITE
                            | wgpu_hal::BufferUses::BOTTOM_LEVEL_ACCELERATION_STRUCTURE_INPUT
                            | wgpu_hal::BufferUses::STORAGE_READ,
                        memory_flags: wgpu_hal::MemoryFlags::empty(),
                    })
                    .unwrap()
            };
            write_buffer(content, device, &buffer);
            buffer
        };
        let index_buffer = {
            let content = cast_slice(&all_indices);
            let size = size_of_val(content);
            let buffer = unsafe {
                device
                    .create_buffer(&wgpu_hal::BufferDescriptor {
                        label: Some(&format!("{:?} Index Buffer", file_name)),
                        size: size as u64,
                        usage: wgpu_hal::BufferUses::INDEX
                            | wgpu_hal::BufferUses::MAP_WRITE
                            | wgpu_hal::BufferUses::BOTTOM_LEVEL_ACCELERATION_STRUCTURE_INPUT
                            | wgpu_hal::BufferUses::STORAGE_READ,
                        memory_flags: wgpu_hal::MemoryFlags::empty(),
                    })
                    .unwrap()
            };
            write_buffer(content, device, &buffer);
            buffer
        };
        let vertex_num = all_vertices.len() as u32;
        materials.push(Material {
            name: m.name.clone(),
            diffuse_texture,
            vertex_buffer,
            index_buffer,
            vertex_num,
            index_num: all_indices.len() as u32,
            vertices: all_vertices,
            indices: all_indices,
        })
    }

    let blas_triangles = materials.par_iter().map(|x| to_blas_triangles(x)).collect::<Vec<_>>();
    let blas_entries = wgpu_hal::AccelerationStructureEntries::Triangles(blas_triangles);
    let blas_sizes = unsafe {
        device.get_acceleration_structure_build_sizes(&wgpu_hal::GetAccelerationStructureBuildSizesDescriptor {
            entries: &blas_entries,
            flags: wgpu_hal::AccelerationStructureBuildFlags::empty(),
        })
    };
    let blas = unsafe {
        device.create_acceleration_structure(&wgpu_hal::AccelerationStructureDescriptor {
            label: Some("blas"),
            size: blas_sizes.acceleration_structure_size,
            format: wgpu_hal::AccelerationStructureFormat::BottomLevel,
        })
    }
    .unwrap();

    let instances_from_model = instances_orig
        .par_iter()
        .map(|x| unsafe {
            AccelerationStructureInstance::new(
                &Affine3A::from_mat4(Mat4::from_cols_array_2d(&x.to_raw().model)),
                model_arena.rt_index_offset(),
                0xff,
                0,
                0,
                device.get_acceleration_structure_device_address(&blas),
            )
        })
        .collect::<Vec<_>>();
    tlas_app.instances.extend(instances_from_model);
    let instances_buffer_size = size_of_val(tlas_app.instances.as_slice());
    let instances_buffer = unsafe {
        let instances_buffer = device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: Some("instances_buffer"),
                size: instances_buffer_size as u64,
                usage: wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::TOP_LEVEL_ACCELERATION_STRUCTURE_INPUT,
                memory_flags: wgpu_hal::MemoryFlags::TRANSIENT | wgpu_hal::MemoryFlags::PREFER_COHERENT,
            })
            .unwrap();
        write_buffer(cast_slice(&tlas_app.instances), device, &instances_buffer);
        instances_buffer
    };
    let tlas_entries = wgpu_hal::AccelerationStructureEntries::Instances(wgpu_hal::AccelerationStructureInstances {
        buffer: Some(&instances_buffer),
        count: tlas_app.instances.len() as u32,
        offset: 0,
    });
    let tlas_sizes = unsafe {
        device.get_acceleration_structure_build_sizes(&wgpu_hal::GetAccelerationStructureBuildSizesDescriptor {
            entries: &tlas_entries,
            flags: wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE,
        })
    };
    let tlas = unsafe {
        device
            .create_acceleration_structure(&wgpu_hal::AccelerationStructureDescriptor {
                label: Some("tlas"),
                size: tlas_sizes.acceleration_structure_size,
                format: wgpu_hal::AccelerationStructureFormat::TopLevel,
            })
            .unwrap()
    };
    let top_scratch_buffer = unsafe {
        device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: Some("scratch buffer"),
                size: tlas_sizes.build_scratch_size,
                usage: wgpu_hal::BufferUses::ACCELERATION_STRUCTURE_SCRATCH,
                memory_flags: wgpu_hal::MemoryFlags::empty(),
            })
            .unwrap()
    };
    let bottom_scratch_buffer = unsafe {
        device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: Some("scratch buffer"),
                size: blas_sizes.build_scratch_size,
                usage: wgpu_hal::BufferUses::ACCELERATION_STRUCTURE_SCRATCH,
                memory_flags: wgpu_hal::MemoryFlags::empty(),
            })
            .unwrap()
    };
    unsafe {
        let old_tlas = replace(&mut tlas_app.tlas, tlas);
        device.destroy_acceleration_structure(old_tlas);
        let old_top_scratch = replace(&mut tlas_app.scratch, top_scratch_buffer);
        device.destroy_buffer(old_top_scratch);
    }
    unsafe {
        command_encoder
            .begin_encoding(Some(&format!("Building AS for {}", file_name)))
            .unwrap();
        command_encoder.place_acceleration_structure_barrier(wgpu_hal::AccelerationStructureBarrier {
            usage: wgpu_hal::AccelerationStructureUses::empty()..wgpu_hal::AccelerationStructureUses::BUILD_INPUT,
        });
        command_encoder.build_acceleration_structures(
            1,
            [wgpu_hal::BuildAccelerationStructureDescriptor {
                mode: wgpu_hal::AccelerationStructureBuildMode::Build,
                flags: wgpu_hal::AccelerationStructureBuildFlags::PREFER_FAST_TRACE
                    | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_COMPACTION
                    | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE,
                destination_acceleration_structure: &blas,
                scratch_buffer: &bottom_scratch_buffer,
                entries: &blas_entries,
                source_acceleration_structure: None,
                scratch_buffer_offset: 0,
            }],
        );
        command_encoder.place_acceleration_structure_barrier(wgpu_hal::AccelerationStructureBarrier {
            usage: wgpu_hal::AccelerationStructureUses::BUILD_OUTPUT..wgpu_hal::AccelerationStructureUses::SHADER_INPUT,
        });
    }
    submit_and_wait(command_encoder, device, queue).unwrap();
    unsafe {
        command_encoder
            .begin_encoding(Some(&format!("Building AS for {}", file_name)))
            .unwrap();
        command_encoder.place_acceleration_structure_barrier(wgpu_hal::AccelerationStructureBarrier {
            usage: wgpu_hal::AccelerationStructureUses::empty()..wgpu_hal::AccelerationStructureUses::BUILD_INPUT,
        });
        command_encoder.build_acceleration_structures(
            1,
            [wgpu_hal::BuildAccelerationStructureDescriptor {
                mode: wgpu_hal::AccelerationStructureBuildMode::Build,
                flags: wgpu_hal::AccelerationStructureBuildFlags::PREFER_FAST_TRACE
                    | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE
                    | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_COMPACTION,
                destination_acceleration_structure: &tlas_app.tlas,
                scratch_buffer: &tlas_app.scratch,
                entries: &tlas_entries,
                source_acceleration_structure: None,
                scratch_buffer_offset: 0,
            }],
        );
        command_encoder.place_acceleration_structure_barrier(wgpu_hal::AccelerationStructureBarrier {
            usage: wgpu_hal::AccelerationStructureUses::BUILD_OUTPUT..wgpu_hal::AccelerationStructureUses::SHADER_INPUT,
        });
        submit_and_wait(command_encoder, device, queue).unwrap();
    }
    let old_instance_buffer = replace(&mut tlas_app.instances_buffer, instances_buffer);
    unsafe {
        device.destroy_buffer(old_instance_buffer);
    }

    // Ok(Model {
    //     materials,
    //     blas: BlasContainer {
    //         blas,
    //         scratch: bottom_scratch_buffer,
    //     },
    // });
    let material_count = materials.len();
    model_arena.push(
        Model {
            materials,
            blas: BlasContainer {
                blas,
                scratch: bottom_scratch_buffer,
            },
        },
        material_count as u32,
    );
    Ok(())
}

pub fn load_gltf(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut wgpu_hal::vulkan::Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    tlas_app: &mut TlasContainer,
    id_offset: u32,
) -> Vec<GltfModel> {
    let (doc, buffers, images) = gltf::import("./res/doll.glb").unwrap();
    let mut my_meshes = vec![];
    let node = doc.nodes().find(|x| x.skin().is_some()).unwrap();
    let t = node.mesh().unwrap();
    let skin = node.skin().unwrap();
    let joints: Vec<_> = skin.joints().collect();
    let mut my_materials = vec![];
    for i in doc.materials() {
        let meshes = t.primitives().filter(|x| x.material().index() == i.index());
        let mut vertices = vec![];
        let mut indices = vec![];
        let mut ind_offset = 0;
        for y in meshes {
            let reader = y.reader(|f| Some(buffers.get(f.index()).unwrap().0.as_slice()));
            let positions = reader.read_positions().unwrap().collect::<Vec<_>>();
            let normals = reader.read_normals().unwrap().collect::<Vec<_>>();
            let tex_coord = reader.read_tex_coords(0).unwrap().into_f32().collect::<Vec<_>>();
            let joints = reader.read_joints(0).unwrap().into_u16().collect::<Vec<_>>();
            let weights = reader.read_weights(0).unwrap().into_f32().collect::<Vec<_>>();
            for p in 0..positions.len() {
                vertices.push(ModelVertex {
                    position: positions[p],
                    tex_coords: tex_coord[p],
                    normal: normals[p],
                    joints: joints[p],
                    weights: weights[p],
                })
            }
            let r_indices = reader.read_indices().unwrap().into_u32();
            for o in r_indices {
                indices.push(o + ind_offset)
            }
            ind_offset += indices.len() as u32;
        }
        if vertices.is_empty() {
            continue;
        };
        let texture = if let Some(image) = i.pbr_metallic_roughness().base_color_texture() {
            let image_data = images.get(image.texture().source().index()).unwrap();
            let diffuse_texture = Texture::from_size(
                device,
                queue,
                (image_data.width, image_data.height),
                None,
                command_encoder,
                None,
                wgpu_types::TextureFormat::Rgba8UnormSrgb,
            )
            .unwrap();
            write_texture(
                image_data.pixels.as_slice(),
                device,
                &diffuse_texture,
                wgpu_types::Origin2d::ZERO,
                command_encoder,
                queue,
            );
            diffuse_texture
        } else {
            let diffuse_texture = Texture::from_size(
                device,
                queue,
                (1, 1),
                None,
                command_encoder,
                None,
                wgpu_types::TextureFormat::Rgba8UnormSrgb,
            )
            .unwrap();
            write_texture(
                &[200, 200, 200, 255],
                device,
                &diffuse_texture,
                wgpu_types::Origin2d::ZERO,
                command_encoder,
                queue,
            );
            diffuse_texture
        };
        let index_buffer = unsafe {
            let buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: None,
                    size: size_of_val(indices.as_slice()) as u64,
                    usage: wgpu_hal::BufferUses::INDEX
                        | wgpu_hal::BufferUses::MAP_WRITE
                        | wgpu_hal::BufferUses::BOTTOM_LEVEL_ACCELERATION_STRUCTURE_INPUT
                        | wgpu_hal::BufferUses::STORAGE_READ_WRITE,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            write_buffer(cast_slice(indices.as_slice()), device, &buffer);
            buffer
        };
        let vertex_buffer = unsafe {
            let buffer = device
                .create_buffer(&wgpu_hal::BufferDescriptor {
                    label: None,
                    size: size_of_val(vertices.as_slice()) as u64,
                    usage: wgpu_hal::BufferUses::VERTEX
                        | wgpu_hal::BufferUses::MAP_WRITE
                        | wgpu_hal::BufferUses::BOTTOM_LEVEL_ACCELERATION_STRUCTURE_INPUT
                        | wgpu_hal::BufferUses::STORAGE_READ_WRITE,
                    memory_flags: wgpu_hal::MemoryFlags::empty(),
                })
                .unwrap();
            write_buffer(cast_slice(vertices.as_slice()), device, &buffer);
            buffer
        };
        my_materials.push(Material {
            name: "".to_string(),
            diffuse_texture: texture,
            vertex_buffer,
            index_buffer,
            vertex_num: vertices.len() as _,
            index_num: indices.len() as _,
            vertices,
            indices,
        })
    }
    let blas_triangles = my_materials.par_iter().map(|x| to_blas_triangles(x)).collect::<Vec<_>>();
    let blas_entries = wgpu_hal::AccelerationStructureEntries::Triangles(blas_triangles);
    let blas_sizes = unsafe {
        device.get_acceleration_structure_build_sizes(&wgpu_hal::GetAccelerationStructureBuildSizesDescriptor {
            entries: &blas_entries,
            flags: wgpu_hal::AccelerationStructureBuildFlags::empty(),
        })
    };
    let blas = unsafe {
        device.create_acceleration_structure(&wgpu_hal::AccelerationStructureDescriptor {
            label: Some("blas"),
            size: blas_sizes.acceleration_structure_size,
            format: wgpu_hal::AccelerationStructureFormat::BottomLevel,
        })
    }
    .unwrap();
    let bottom_scratch_buffer = unsafe {
        device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: Some("scratch buffer"),
                size: blas_sizes.build_scratch_size,
                usage: wgpu_hal::BufferUses::ACCELERATION_STRUCTURE_SCRATCH,
                memory_flags: wgpu_hal::MemoryFlags::empty(),
            })
            .unwrap()
    };
    let mut skeleton = vec![];
    let skin_reader = skin.reader(|x| Some(buffers.get(x.index()).unwrap()));
    for (u, i) in skin_reader.read_inverse_bind_matrices().unwrap().enumerate() {
        let parent_calculator = if let Some(r) = u.checked_sub(1) { r } else { 0 };
        skeleton.push(Bone {
            inv_bind_matrix: Mat4::from_cols_array_2d(&i),
            transform_matrix: Mat4::from_cols_array_2d(&joints[u].transform().matrix()),
            parent: joints[parent_calculator].index() as u32,
        })
    }
    my_meshes.push(GltfModel {
        materials: my_materials,
        skinned_vertices: vec![],
        blas: BlasContainer {
            blas,
            scratch: bottom_scratch_buffer,
        },
        skeleton: Some(skeleton),
    });
    let mut previous_material_len = 0u32;
    for i in my_meshes.iter() {
        let GltfModel { blas, materials, .. } = i;
        unsafe {
            tlas_app.instances.push(AccelerationStructureInstance::new(
                &Affine3A::from_translation(Vec3::new(0.0, 4.0, 0.0)),
                id_offset + previous_material_len,
                0xff,
                0,
                0,
                device.get_acceleration_structure_device_address(&blas.blas),
            ))
        }
        previous_material_len += materials.len() as u32;
    }
    let instances_buffer_size = size_of_val(tlas_app.instances.as_slice());
    let instances_buffer = unsafe {
        let instances_buffer = device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: Some("instances_buffer"),
                size: instances_buffer_size as u64,
                usage: wgpu_hal::BufferUses::MAP_WRITE | wgpu_hal::BufferUses::TOP_LEVEL_ACCELERATION_STRUCTURE_INPUT,
                memory_flags: wgpu_hal::MemoryFlags::TRANSIENT | wgpu_hal::MemoryFlags::PREFER_COHERENT,
            })
            .unwrap();
        write_buffer(cast_slice(&tlas_app.instances), device, &instances_buffer);
        instances_buffer
    };
    let old_instance_buffer = replace(&mut tlas_app.instances_buffer, instances_buffer);
    unsafe {
        device.destroy_buffer(old_instance_buffer);
    }
    let tlas_entries = wgpu_hal::AccelerationStructureEntries::Instances(wgpu_hal::AccelerationStructureInstances {
        buffer: Some(&tlas_app.instances_buffer),
        count: tlas_app.instances.len() as u32,
        offset: 0,
    });
    let tlas_sizes = unsafe {
        device.get_acceleration_structure_build_sizes(&wgpu_hal::GetAccelerationStructureBuildSizesDescriptor {
            entries: &tlas_entries,
            flags: wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE,
        })
    };
    let tlas = unsafe {
        device
            .create_acceleration_structure(&wgpu_hal::AccelerationStructureDescriptor {
                label: Some("tlas"),
                size: tlas_sizes.acceleration_structure_size,
                format: wgpu_hal::AccelerationStructureFormat::TopLevel,
            })
            .unwrap()
    };
    let top_scratch_buffer = unsafe {
        device
            .create_buffer(&wgpu_hal::BufferDescriptor {
                label: Some("scratch buffer"),
                size: tlas_sizes.build_scratch_size,
                usage: wgpu_hal::BufferUses::ACCELERATION_STRUCTURE_SCRATCH,
                memory_flags: wgpu_hal::MemoryFlags::empty(),
            })
            .unwrap()
    };
    unsafe {
        let old_tlas = replace(&mut tlas_app.tlas, tlas);
        device.destroy_acceleration_structure(old_tlas);
        let old_top_scratch = replace(&mut tlas_app.scratch, top_scratch_buffer);
        device.destroy_buffer(old_top_scratch);
    }
    unsafe { command_encoder.begin_encoding(Some(&format!("Building AS for {:?}", doc))).unwrap() };
    unsafe {
        command_encoder.place_acceleration_structure_barrier(wgpu_hal::AccelerationStructureBarrier {
            usage: wgpu_hal::AccelerationStructureUses::empty()..wgpu_hal::AccelerationStructureUses::BUILD_INPUT,
        });
    }
    unsafe {
        for mesh in my_meshes.iter() {
            let GltfModel { materials, blas, .. } = mesh;
            let blas_triangles = materials.par_iter().map(|x| to_blas_triangles(x)).collect::<Vec<_>>();
            let blas_entries = wgpu_hal::AccelerationStructureEntries::Triangles(blas_triangles);
            command_encoder.build_acceleration_structures(
                1,
                [wgpu_hal::BuildAccelerationStructureDescriptor {
                    mode: wgpu_hal::AccelerationStructureBuildMode::Build,
                    flags: wgpu_hal::AccelerationStructureBuildFlags::PREFER_FAST_TRACE
                        | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_COMPACTION
                        | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE,
                    destination_acceleration_structure: &blas.blas,
                    scratch_buffer: &blas.scratch,
                    entries: &blas_entries,
                    source_acceleration_structure: None,
                    scratch_buffer_offset: 0,
                }],
            );
        }
    }
    unsafe {
        command_encoder.build_acceleration_structures(
            1,
            [wgpu_hal::BuildAccelerationStructureDescriptor {
                mode: wgpu_hal::AccelerationStructureBuildMode::Build,
                flags: wgpu_hal::AccelerationStructureBuildFlags::PREFER_FAST_TRACE
                    | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_UPDATE
                    | wgpu_hal::AccelerationStructureBuildFlags::ALLOW_COMPACTION,
                destination_acceleration_structure: &tlas_app.tlas,
                scratch_buffer: &tlas_app.scratch,
                entries: &tlas_entries,
                source_acceleration_structure: None,
                scratch_buffer_offset: 0,
            }],
        );
        command_encoder.place_acceleration_structure_barrier(wgpu_hal::AccelerationStructureBarrier {
            usage: wgpu_hal::AccelerationStructureUses::BUILD_OUTPUT..wgpu_hal::AccelerationStructureUses::SHADER_INPUT,
        });
        submit_and_wait(command_encoder, device, queue).unwrap();
    }
    my_meshes
}

pub async fn load_string(file_name: &str) -> anyhow::Result<String> {
    let path = std::path::Path::new(env!("OUT_DIR")).join("res").join(file_name);
    let txt = std::fs::read_to_string(path)?;

    Ok(txt)
}

pub async fn load_binary(file_name: &str) -> anyhow::Result<Vec<u8>> {
    let path = std::path::Path::new(env!("OUT_DIR")).join("res").join(file_name);
    let data = std::fs::read(path)?;
    Ok(data)
}
pub async fn load_texture(
    file_name: &str,
    device: &wgpu_hal::vulkan::Device,
    queue: &mut wgpu_hal::vulkan::Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
) -> anyhow::Result<Texture> {
    let data = load_binary(file_name).await?;
    unsafe { Texture::from_bytes(device, queue, &data, file_name, command_encoder) }
}
pub fn make_spirv_raw(data: &[u8]) -> std::borrow::Cow<[u32]> {
    use std::borrow::Cow;
    use std::mem::align_of;
    use std::ptr::copy_nonoverlapping;
    const MAGIC_NUMBER: u32 = 0x0723_0203;
    assert_eq!(data.len() % size_of::<u32>(), 0, "data size is not a multiple of 4");

    //If the data happens to be aligned, directly use the byte array,
    // otherwise copy the byte array in an owned vector and use that instead.
    let words = if data.as_ptr().align_offset(align_of::<u32>()) == 0 {
        let (pre, words, post) = unsafe { data.align_to::<u32>() };
        debug_assert!(pre.is_empty());
        debug_assert!(post.is_empty());
        Cow::from(words)
    } else {
        let mut words = vec![0u32; data.len() / size_of::<u32>()];
        unsafe {
            copy_nonoverlapping(data.as_ptr(), words.as_mut_ptr() as *mut u8, data.len());
        }
        Cow::from(words)
    };

    assert_eq!(
        words[0], MAGIC_NUMBER,
        "wrong magic word {:x}. Make sure you are using a binary SPIRV file.",
        words[0]
    );

    words
}

#[inline]
pub fn to_blas_triangles(x: &Material) -> AccelerationStructureTriangles<wgpu_hal::vulkan::Api> {
    AccelerationStructureTriangles {
        vertex_buffer: Some(&x.vertex_buffer),
        vertex_format: wgpu_types::VertexFormat::Float32x3,
        first_vertex: 0,
        vertex_count: x.vertex_num,
        vertex_stride: size_of::<ModelVertex>() as u64,
        indices: Some(wgpu_hal::AccelerationStructureTriangleIndices {
            format: wgpu_types::IndexFormat::Uint32,
            buffer: Some(&x.index_buffer),
            offset: 0,
            count: x.index_num,
        }),
        transform: None,
        flags: wgpu_hal::AccelerationStructureGeometryFlags::OPAQUE,
    }
}
