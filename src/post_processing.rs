use std::iter::once;

use wgpu_hal::vulkan::{BindGroup, BindGroupLayout, PipelineLayout, Queue, RenderPipeline, Sampler, ShaderModule};
use wgpu_hal::{CommandEncoder, Device, TextureCopy, TextureUses};
use wgpu_types as wgt;
use wgpu_types::{BindGroupLayoutEntry, Extent3d};

use crate::g_buffers::HelperTextures;
use crate::resources::make_spirv_raw;
use crate::texture::Texture;

const TEXTURE_BINDING: wgt::BindingType = wgt::BindingType::Texture {
    sample_type: wgt::TextureSampleType::Float { filterable: true },
    view_dimension: wgt::TextureViewDimension::D2,
    multisampled: false,
};
const TEXTURE_BINDING_UINT: wgt::BindingType = wgt::BindingType::Texture {
    sample_type: wgt::TextureSampleType::Uint,
    view_dimension: wgt::TextureViewDimension::D2,
    multisampled: false,
};

///Contains textures from previous frames, accessed as `texture2d`
pub struct PostProcessing {
    pub bind_group: Option<BindGroup>,
    pub bind_group_layout: BindGroupLayout,
    pub sampler: Sampler,
    pub pipeline_layout: PipelineLayout,
    pub pipeline: RenderPipeline,
    pub vs: ShaderModule,
    pub fs: ShaderModule,

    index: Option<Texture>,
    albedo: Option<Texture>,
    normal: Option<Texture>,
    position: Option<Texture>,
    direct_light: Option<Texture>,
    depth: Option<Texture>,
    final_color: Option<Texture>,
    indirect_light: Option<Texture>,
    motion: Option<Texture>,
}

impl PostProcessing {
    pub fn resize(
        &mut self,
        device: &wgpu_hal::vulkan::Device,
        queue: &mut Queue,
        encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        dest_texture_view: &wgpu_hal::vulkan::TextureView,
        gui_texture_view: &wgpu_hal::vulkan::TextureView,
        size: Extent3d,
    ) {
        unsafe {
            self.index.take().unwrap().destroy(device);
            self.albedo.take().unwrap().destroy(device);
            self.normal.take().unwrap().destroy(device);
            self.position.take().unwrap().destroy(device);
            self.direct_light.take().unwrap().destroy(device);
            self.depth.take().unwrap().destroy(device);
            self.final_color.take().unwrap().destroy(device);
            self.indirect_light.take().unwrap().destroy(device);
            self.motion.take().unwrap().destroy(device);

            device.destroy_bind_group(self.bind_group.take().unwrap());
        };
        self.create_textures_and_bind_group(device, queue, encoder, size, dest_texture_view, gui_texture_view)
    }
    pub fn new(
        device: &wgpu_hal::vulkan::Device,
        surface_format: wgt::TextureFormat,
        scene_texture_view: &wgpu_hal::vulkan::TextureView,
        gui_texture_view: &wgpu_hal::vulkan::TextureView,
        helper_textures: &HelperTextures,
        camera_bgl: &BindGroupLayout,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        queue: &mut Queue,
        size: Extent3d,
    ) -> Self {
        let post_process_vs = unsafe {
            device.create_shader_module(
                &wgpu_hal::ShaderModuleDescriptor {
                    label: None,
                    runtime_checks: false,
                },
                wgpu_hal::ShaderInput::SpirV(&make_spirv_raw(include_bytes!("../shaders/post_process_vert.spv"))),
            )
        }
        .unwrap();
        let post_process_fs = unsafe {
            device.create_shader_module(
                &wgpu_hal::ShaderModuleDescriptor {
                    label: None,
                    runtime_checks: false,
                },
                wgpu_hal::ShaderInput::SpirV(&make_spirv_raw(include_bytes!("../shaders/post_process_frag.spv"))),
            )
        }
        .unwrap();
        let post_process_bgl = unsafe {
            device.create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                label: None,
                flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                entries: &[
                    BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu_types::ShaderStages::FRAGMENT | wgt::ShaderStages::COMPUTE,
                        ty: wgt::BindingType::Sampler(wgpu_types::SamplerBindingType::Filtering),
                        count: None,
                    },
                    texture_bgl_entry(1),
                    texture_bgl_entry(2),
                    texture_bgl_entry_uint(3),
                    texture_bgl_entry(4),
                    texture_bgl_entry(5),
                    texture_bgl_entry(6),
                    texture_bgl_entry(7),
                    texture_bgl_entry(8),
                    texture_bgl_entry(9),
                    texture_bgl_entry(10),
                    texture_bgl_entry(11),
                ],
            })
        }
        .unwrap();
        let post_process_layout = unsafe {
            device.create_pipeline_layout(&wgpu_hal::PipelineLayoutDescriptor {
                label: None,
                flags: wgpu_hal::PipelineLayoutFlags::empty(),
                bind_group_layouts: &[&post_process_bgl, &helper_textures.bgl, camera_bgl],
                push_constant_ranges: &[],
            })
        }
        .unwrap();
        let post_process_pipeline = unsafe {
            device.create_render_pipeline(&wgpu_hal::RenderPipelineDescriptor {
                label: None,
                layout: &post_process_layout,
                vertex_buffers: &[],
                vertex_stage: wgpu_hal::ProgrammableStage {
                    module: &post_process_vs,
                    entry_point: "main",
                    constants: &Default::default(),
                    zero_initialize_workgroup_memory: false,
                    vertex_pulling_transform: false,
                },
                primitive: Default::default(),
                depth_stencil: None,
                multisample: Default::default(),
                fragment_stage: Some(wgpu_hal::ProgrammableStage {
                    module: &post_process_fs,
                    entry_point: "main",
                    constants: &Default::default(),
                    zero_initialize_workgroup_memory: false,
                    vertex_pulling_transform: false,
                }),
                color_targets: &[Some(wgpu_types::ColorTargetState {
                    format: surface_format,
                    blend: None,
                    write_mask: Default::default(),
                })],
                multiview: None,
                cache: None,
            })
        }
        .unwrap();
        let post_process_general_sampler = unsafe {
            device.create_sampler(&wgpu_hal::SamplerDescriptor {
                label: None,
                address_modes: [wgt::AddressMode::ClampToEdge; 3],
                mag_filter: wgt::FilterMode::Linear,
                min_filter: wgt::FilterMode::Linear,
                mipmap_filter: wgt::FilterMode::Linear,
                lod_clamp: Default::default(),
                compare: None,
                anisotropy_clamp: 16,
                border_color: None,
            })
        }
        .unwrap();

        let mut result = PostProcessing {
            bind_group: None,
            bind_group_layout: post_process_bgl,
            sampler: post_process_general_sampler,
            pipeline_layout: post_process_layout,
            pipeline: post_process_pipeline,
            vs: post_process_vs,
            fs: post_process_fs,

            index: None,
            albedo: None,
            normal: None,
            position: None,
            direct_light: None,
            depth: None,
            final_color: None,
            indirect_light: None,
            motion: None,
        };
        result.create_textures_and_bind_group(device, queue, command_encoder, size, scene_texture_view, gui_texture_view);
        result
    }
    pub fn destroy(mut self, device: &wgpu_hal::vulkan::Device) {
        unsafe {
            device.destroy_sampler(self.sampler);
            device.destroy_render_pipeline(self.pipeline);
            device.destroy_pipeline_layout(self.pipeline_layout);

            self.index.take().unwrap().destroy(device);
            self.albedo.take().unwrap().destroy(device);
            self.normal.take().unwrap().destroy(device);
            self.position.take().unwrap().destroy(device);
            self.direct_light.take().unwrap().destroy(device);
            self.final_color.take().unwrap().destroy(device);
            self.depth.take().unwrap().destroy(device);
            self.indirect_light.take().unwrap().destroy(device);
            self.motion.take().unwrap().destroy(device);

            // self.get_all_tex().map(|x|assert!(x == None));

            device.destroy_bind_group(self.bind_group.take().unwrap());
            device.destroy_bind_group_layout(self.bind_group_layout);
            device.destroy_shader_module(self.fs);
            device.destroy_shader_module(self.vs);
        }
    }
    fn create_textures_and_bind_group(
        &mut self,
        device: &wgpu_hal::vulkan::Device,
        queue: &mut Queue,
        ce: &mut wgpu_hal::vulkan::CommandEncoder,
        size: Extent3d,
        scene_tex_view: &wgpu_hal::vulkan::TextureView,
        gui_tex_view: &wgpu_hal::vulkan::TextureView,
    ) {
        self.index = Some(integer_texture(device, queue, ce, size).unwrap());
        self.albedo = Some(color_texture(device, queue, ce, size).unwrap());
        self.normal = Some(color_texture_8bit(device, queue, ce, size).unwrap());
        self.position = Some(color_texture(device, queue, ce, size).unwrap());
        self.direct_light = Some(color_texture(device, queue, ce, size).unwrap());
        self.depth = Some(depth_texture(device, queue, ce, size).unwrap());
        self.final_color = Some(color_texture(device, queue, ce, size).unwrap());
        self.indirect_light = Some(color_texture_lowp(device, queue, ce, size).unwrap());
        self.motion = Some(color_texture_lowp(device, queue, ce, size).unwrap());

        let mut bind_group_textures = vec![
            wgpu_hal::TextureBinding {
                view: scene_tex_view,
                usage: TextureUses::RESOURCE,
            },
            wgpu_hal::TextureBinding {
                view: gui_tex_view,
                usage: TextureUses::RESOURCE,
            },
        ];
        bind_group_textures.extend_from_slice(&self.get_all_tex().map(|x| wgpu_hal::TextureBinding {
            view: &x.unwrap().view,
            usage: TextureUses::RESOURCE,
        }));
        let post_process_bind_group = unsafe {
            device.create_bind_group(&wgpu_hal::BindGroupDescriptor {
                label: None,
                layout: &self.bind_group_layout,
                buffers: &[],
                samplers: &[&self.sampler],
                textures: bind_group_textures.as_slice(),
                entries: &[
                    vec![wgpu_hal::BindGroupEntry {
                        binding: 0,
                        resource_index: 0,
                        count: 1,
                    }],
                    (1..self.get_all_tex().len() as u32 + 2 /* gui and 3d*/ + 1)
                        .map(|x| wgpu_hal::BindGroupEntry {
                            binding: x,
                            resource_index: x - 1,
                            count: 1,
                        })
                        .collect::<Vec<_>>(),
                ]
                .concat(),
                acceleration_structures: &[],
            })
        }
        .unwrap();
        self.bind_group = Some(post_process_bind_group);
    }
    pub fn index(&self) -> Option<&Texture> {
        self.index.as_ref()
    }
    pub fn albedo(&self) -> Option<&Texture> {
        self.albedo.as_ref()
    }
    pub fn normal(&self) -> Option<&Texture> {
        self.normal.as_ref()
    }
    pub fn position(&self) -> Option<&Texture> {
        self.position.as_ref()
    }
    pub fn direct_light(&self) -> Option<&Texture> {
        self.direct_light.as_ref()
    }
    pub fn depth(&self) -> Option<&Texture> {
        self.depth.as_ref()
    }
    pub fn final_color(&self) -> Option<&Texture> {
        self.final_color.as_ref()
    }
    pub fn indirect_light(&self) -> Option<&Texture> {
        self.indirect_light.as_ref()
    }
    pub fn motion(&self) -> Option<&Texture> {
        self.motion.as_ref()
    }

    /// `COPY_DST to RESOURCE`
    ///
    /// Make copied **to** textures available for sampling
    pub fn write_to_resource(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            self.get_all_tex().iter().for_each(|x| {
                write_to_read(ce, &x.unwrap().texture);
            })
        }
    }
    /// `RESOURCE to COPY_DST`
    ///
    /// Open post-processing textures to transfer
    pub fn resource_to_write(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            self.get_all_tex().iter().for_each(|x| {
                assert_ne!(x.unwrap().uses & TextureUses::COPY_DST, TextureUses::empty());
                read_to_write(ce, &x.unwrap().texture)
            })
        }
    }
    /// Store previous frame in post-processing textures
    pub fn copy_from_pf(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder, help_tex: &HelperTextures) {
        unsafe {
            for (i, u) in self.get_all_tex().iter().zip(help_tex.get_pf_textures()) {
                copy_to_texture(ce, u, i.unwrap())
            }
        }
    }
    fn get_all_tex(&self) -> [Option<&Texture>; 9] {
        [
            self.index(),
            self.albedo(),
            self.normal(),
            self.position(),
            self.direct_light(),
            self.depth(),
            self.final_color(),
            self.indirect_light(),
            self.motion(),
        ]
    }
    pub fn bind_group(&self) -> &BindGroup {
        self.bind_group.as_ref().unwrap()
    }
}
#[inline]
fn color_texture(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(TextureUses::COPY_DST | TextureUses::RESOURCE),
        wgpu_types::TextureFormat::Rgba32Float,
    )
}
#[inline]
fn color_texture_8bit(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(TextureUses::COPY_DST | TextureUses::RESOURCE),
        wgpu_types::TextureFormat::Rgba8Unorm,
    )
}
#[inline]
fn color_texture_lowp(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(TextureUses::COPY_DST | TextureUses::RESOURCE),
        wgpu_types::TextureFormat::Rgba16Float,
    )
}
#[inline]
fn integer_texture(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(TextureUses::COPY_DST | TextureUses::RESOURCE),
        wgpu_types::TextureFormat::R32Uint,
    )
}
#[inline]
unsafe fn write_to_read(command_encoder: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: TextureUses::COPY_DST..TextureUses::RESOURCE,
    }));
}
#[inline]
unsafe fn read_to_write(command_encoder: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: TextureUses::RESOURCE..TextureUses::COPY_DST,
    }));
}
unsafe fn copy_to_texture(command_encoder: &mut wgpu_hal::vulkan::CommandEncoder, src_tex: &Texture, dst_tex: &Texture) {
    command_encoder.copy_texture_to_texture(
        &src_tex.texture,
        TextureUses::COPY_SRC,
        &dst_tex.texture,
        once(TextureCopy {
            src_base: wgpu_hal::TextureCopyBase {
                mip_level: 0,
                array_layer: 0,
                origin: Default::default(),
                aspect: wgpu_hal::FormatAspects::COLOR,
            },
            dst_base: wgpu_hal::TextureCopyBase {
                mip_level: 0,
                array_layer: 0,
                origin: Default::default(),
                aspect: wgpu_hal::FormatAspects::COLOR,
            },
            size: wgpu_hal::CopyExtent {
                width: dst_tex.size.width,
                height: dst_tex.size.height,
                depth: 1,
            },
        }),
    )
}
#[inline]
fn texture_bgl_entry(n: u32) -> BindGroupLayoutEntry {
    BindGroupLayoutEntry {
        binding: n,
        visibility: wgpu_types::ShaderStages::FRAGMENT | wgt::ShaderStages::COMPUTE,
        ty: TEXTURE_BINDING,
        count: None,
    }
}
fn texture_bgl_entry_uint(n: u32) -> BindGroupLayoutEntry {
    BindGroupLayoutEntry {
        binding: n,
        visibility: wgpu_types::ShaderStages::FRAGMENT | wgt::ShaderStages::COMPUTE,
        ty: TEXTURE_BINDING_UINT,
        count: None,
    }
}
fn depth_texture(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(TextureUses::COPY_DST | TextureUses::RESOURCE),
        wgpu_types::TextureFormat::R32Float,
    )
}
