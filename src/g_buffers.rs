use std::iter::once;
use std::mem::replace;

use wgpu_hal::vulkan::Queue;
use wgpu_hal::{CommandEncoder, Device};
use wgpu_types as wgt;
use wgpu_types::{Extent3d, ShaderStages};

use crate::texture::Texture;

const INDEX_TEXTURE_BINDING_TYPE: wgt::BindingType = wgt::BindingType::StorageTexture {
    access: wgt::StorageTextureAccess::ReadWrite,
    format: wgt::TextureFormat::R32Uint,
    view_dimension: wgt::TextureViewDimension::D2,
};
const COLOR_TEXTURE_BINDING_TYPE: wgt::BindingType = wgt::BindingType::StorageTexture {
    access: wgt::StorageTextureAccess::ReadWrite,
    format: wgt::TextureFormat::Rgba32Float,
    view_dimension: wgt::TextureViewDimension::D2,
};

const COLOR_TEXTURE_BINDING_TYPE_8BIT: wgt::BindingType = wgt::BindingType::StorageTexture {
    access: wgt::StorageTextureAccess::ReadWrite,
    format: wgt::TextureFormat::Rgba8Unorm,
    view_dimension: wgt::TextureViewDimension::D2,
};
const COLOR_TEXTURE_BINDING_TYPE_16BIT: wgt::BindingType = wgt::BindingType::StorageTexture {
    access: wgt::StorageTextureAccess::ReadWrite,
    format: wgt::TextureFormat::Rgba16Float,
    view_dimension: wgt::TextureViewDimension::D2,
};
const DEPTH_TEXTURE_BINDING_TYPE: wgt::BindingType = wgt::BindingType::StorageTexture {
    access: wgt::StorageTextureAccess::ReadWrite,
    format: wgt::TextureFormat::R32Float,
    view_dimension: wgt::TextureViewDimension::D2,
};

///Contains textures from path tracer and storage textures to store them in "past frame"
pub struct HelperTextures {
    pub bgl: wgpu_hal::vulkan::BindGroupLayout,
    pub bind_group: wgpu_hal::vulkan::BindGroup,

    pub index: Texture,
    pub albedo: Texture,
    pub normal: Texture,
    pub position: Texture,
    pub direct_light: Texture,
    pub depth: Texture,
    pub indirect_light: Texture,

    pub pf_index: Texture,
    pub pf_albedo: Texture,
    pub pf_normal: Texture,
    pub pf_position: Texture,
    pub pf_direct_light: Texture,
    pub pf_depth: Texture,
    pub pf_final_color: Texture,
    pub pf_indirect_light: Texture,
    pub pf_motion: Texture,
}

impl HelperTextures {
    pub fn new(
        device: &wgpu_hal::vulkan::Device,
        queue: &mut Queue,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        size: Extent3d,
    ) -> Self {
        let index_texture = Texture::from_size(
            device,
            queue,
            (size.width, size.height),
            None,
            command_encoder,
            Some(wgpu_hal::TextureUses::STORAGE_READ_WRITE),
            wgpu_types::TextureFormat::R32Uint,
        )
        .unwrap();
        let albedo = color_texture(device, queue, command_encoder, size).unwrap();
        let normal = color_texture_8bit(device, queue, command_encoder, size).unwrap();
        let position = color_texture(device, queue, command_encoder, size).unwrap();
        let direct_light = color_texture(device, queue, command_encoder, size).unwrap();
        let depth = depth_texture(device, queue, command_encoder, size).unwrap();
        let indirect_light = color_texture_lowp(device, queue, command_encoder, size).unwrap();

        let pf_index_texture = Texture::from_size(
            device,
            queue,
            (size.width, size.height),
            None,
            command_encoder,
            Some(wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::COPY_SRC),
            wgpu_types::TextureFormat::R32Uint,
        )
        .unwrap();
        let pf_albedo = color_texture(device, queue, command_encoder, size).unwrap();
        let pf_normal = color_texture_8bit(device, queue, command_encoder, size).unwrap();
        let pf_position = color_texture(device, queue, command_encoder, size).unwrap();
        let pf_direct_light = color_texture(device, queue, command_encoder, size).unwrap();
        let pf_depth = depth_texture(device, queue, command_encoder, size).unwrap();
        let pf_final_color = color_texture(device, queue, command_encoder, size).unwrap();
        let pf_indirect_light = color_texture_lowp(device, queue, command_encoder, size).unwrap();
        let pf_motion = color_texture_lowp(device, queue, command_encoder, size).unwrap();

        let g_buffer_bgl = unsafe {
            device.create_bind_group_layout(&wgpu_hal::BindGroupLayoutDescriptor {
                label: None,
                flags: wgpu_hal::BindGroupLayoutFlags::empty(),
                entries: &[
                    wgt::BindGroupLayoutEntry {
                        //index
                        binding: 0,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: INDEX_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //albedo
                        binding: 1,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //normal
                        binding: 2,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: COLOR_TEXTURE_BINDING_TYPE_8BIT,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //position
                        binding: 3,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //direct_light
                        binding: 4,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //depth
                        binding: 5,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: DEPTH_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //indirect color
                        binding: 6,
                        visibility: ShaderStages::COMPUTE | ShaderStages::FRAGMENT,
                        ty: COLOR_TEXTURE_BINDING_TYPE_16BIT,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //index
                        binding: 7,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: INDEX_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //albedo
                        binding: 8,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //normal
                        binding: 9,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE_8BIT,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //position
                        binding: 10,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //direct_light
                        binding: 11,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //depth
                        binding: 12,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: DEPTH_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //final color
                        binding: 13,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //indirect light
                        binding: 14,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE_16BIT,
                        count: None,
                    },
                    wgt::BindGroupLayoutEntry {
                        //motion
                        binding: 15,
                        visibility: ShaderStages::FRAGMENT | ShaderStages::COMPUTE,
                        ty: COLOR_TEXTURE_BINDING_TYPE_16BIT,
                        count: None,
                    },
                ],
            })
        }
        .unwrap();
        let g_buffer_bind_group = unsafe {
            device.create_bind_group(&wgpu_hal::BindGroupDescriptor {
                label: None,
                layout: &g_buffer_bgl,
                buffers: &[],
                samplers: &[],
                textures: &[
                    wgpu_hal::TextureBinding {
                        view: &index_texture.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &albedo.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &normal.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &position.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &direct_light.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &depth.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &indirect_light.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_index_texture.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_albedo.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_normal.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_position.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_direct_light.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_depth.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_final_color.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_indirect_light.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                    wgpu_hal::TextureBinding {
                        view: &pf_motion.view,
                        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE,
                    },
                ],
                entries: &(0..16) //Make ```x``` consecutive texture bindings
                    .map(|x| wgpu_hal::BindGroupEntry {
                        binding: x,
                        resource_index: x,
                        count: 1,
                    })
                    .collect::<Vec<_>>(),
                acceleration_structures: &[],
            })
        }
        .unwrap();
        Self {
            bgl: g_buffer_bgl,
            bind_group: g_buffer_bind_group,

            index: index_texture,
            albedo,
            normal,
            position,
            direct_light,
            depth,
            indirect_light,

            pf_index: pf_index_texture,
            pf_albedo,
            pf_normal,
            pf_position,
            pf_direct_light,
            pf_depth,
            pf_final_color,
            pf_indirect_light,
            pf_motion,
        }
    }
    pub fn resize(
        &mut self,
        device: &wgpu_hal::vulkan::Device,
        queue: &mut Queue,
        command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
        resolution: Extent3d,
    ) {
        let new = Self::new(device, queue, command_encoder, resolution);
        let old = replace(self, new);
        old.destroy(device);
    }
    pub fn destroy(self, device: &wgpu_hal::vulkan::Device) {
        unsafe { device.destroy_bind_group(self.bind_group) }
        [
            self.index,
            self.albedo,
            self.normal,
            self.direct_light,
            self.position,
            self.depth,
            self.indirect_light,
            self.pf_index,
            self.pf_albedo,
            self.pf_normal,
            self.pf_direct_light,
            self.pf_position,
            self.pf_depth,
            self.pf_final_color,
            self.pf_indirect_light,
            self.pf_motion,
        ]
        .into_iter()
        .for_each(|x| x.destroy(device));
        unsafe { device.destroy_bind_group_layout(self.bgl) }
    }

    /// Transition from STORAGE_READ_WRITE to STORAGE_READ
    ///
    /// Only path tracer framebuffers
    ///
    /// Transition g buffers to read them from post-processing
    ///
    /// Use when recording command buffer
    pub fn write_to_read(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for i in self.get_g_buffers() {
                write_to_read(ce, &i.texture)
            }
        }
    }
    /// Transition from `STORAGE_READ` to `STORAGE_READ_WRITE`
    ///
    /// Only path tracer framebuffers to make writable from compute shader
    ///
    /// Use when recording command buffer
    pub fn read_to_write(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for i in self.get_g_buffers() {
                read_to_write(ce, &i.texture)
            }
        }
    }
    /// Transition **copied** pf textures to make compute shader not complain
    ///
    /// ```pf COPY_SRC to STORAGE_READ_WRITE```
    pub fn pf_src_to_storage(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for i in self.get_pf_textures() {
                src_to_storage(ce, &i.texture)
            }
        }
    }
    ///Make previous frame buffers source to copy from
    ///
    /// We generated a frame, and now we need to store it in post-processing
    ///
    /// ```pf STORAGE_READ_WRITE to COPY_SRC```
    pub fn pf_storage_to_src(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for i in self.get_pf_textures() {
                storage_to_src(ce, &i.texture)
            }
        }
    }
    pub fn _pf_storage_to_r(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for i in self.get_pf_textures() {
                storage_rw_to_storage_r(ce, &i.texture)
            }
        }
    }
    pub fn _pf_storage_to_rw(&self, ce: &mut wgpu_hal::vulkan::CommandEncoder) {
        unsafe {
            for i in self.get_pf_textures() {
                storage_r_to_storage_rw(ce, &i.texture)
            }
        }
    }
    pub fn get_g_buffers(&self) -> [&Texture; 7] {
        [
            &self.index,
            &self.albedo,
            &self.normal,
            &self.position,
            &self.direct_light,
            &self.depth,
            &self.indirect_light,
        ]
    }
    pub fn get_pf_textures(&self) -> [&Texture; 9] {
        [
            &self.pf_index,
            &self.pf_albedo,
            &self.pf_normal,
            &self.pf_position,
            &self.pf_direct_light,
            &self.pf_depth,
            &self.pf_final_color,
            &self.pf_indirect_light,
            &self.pf_motion,
        ]
    }
}

#[inline]
fn color_texture(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    ce: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        ce,
        Some(wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::COPY_SRC),
        wgpu_types::TextureFormat::Rgba32Float,
    )
}
#[inline]
fn color_texture_8bit(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::COPY_SRC),
        wgpu_types::TextureFormat::Rgba8Unorm,
    )
}
#[inline]
fn color_texture_lowp(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::COPY_SRC),
        wgpu_types::TextureFormat::Rgba16Float,
    )
}
#[inline]
fn depth_texture(
    device: &wgpu_hal::vulkan::Device,
    queue: &mut Queue,
    command_encoder: &mut wgpu_hal::vulkan::CommandEncoder,
    size: Extent3d,
) -> anyhow::Result<Texture> {
    Texture::from_size(
        device,
        queue,
        (size.width, size.height),
        None,
        command_encoder,
        Some(wgpu_hal::TextureUses::STORAGE_READ_WRITE | wgpu_hal::TextureUses::COPY_SRC),
        wgpu_types::TextureFormat::R32Float,
    )
}

#[inline]
unsafe fn write_to_read(ce: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    ce.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE..wgpu_hal::TextureUses::STORAGE_READ,
    }));
}
#[inline]
unsafe fn read_to_write(command_encoder: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    command_encoder.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: wgpu_hal::TextureUses::STORAGE_READ..wgpu_hal::TextureUses::STORAGE_READ_WRITE,
    }));
}
#[inline]
unsafe fn storage_to_src(ce: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    ce.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE..wgpu_hal::TextureUses::COPY_SRC,
    }));
}
#[inline]
unsafe fn src_to_storage(ce: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    ce.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: wgpu_hal::TextureUses::COPY_SRC..wgpu_hal::TextureUses::STORAGE_READ_WRITE,
    }));
}
#[inline]
unsafe fn storage_rw_to_storage_r(ce: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    ce.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: wgpu_hal::TextureUses::STORAGE_READ_WRITE..wgpu_hal::TextureUses::STORAGE_READ,
    }));
}
#[inline]
unsafe fn storage_r_to_storage_rw(ce: &mut wgpu_hal::vulkan::CommandEncoder, texture: &wgpu_hal::vulkan::Texture) {
    ce.transition_textures(once(wgpu_hal::TextureBarrier {
        texture,
        range: Default::default(),
        usage: wgpu_hal::TextureUses::STORAGE_READ..wgpu_hal::TextureUses::STORAGE_READ_WRITE,
    }));
}
