#version 460

const vec2 vertices[3]=vec2[3](vec2(-1,-1), vec2(3,-1), vec2(-1, 3));

layout(location = 0) out vec2 texcoords; // texcoords are in the normalized [0,1] range for the viewport-filling quad part of the triangle
void main() {
    gl_Position = vec4(vertices[gl_VertexIndex],0,1);
    texcoords = 0.5 * vec2(gl_Position.x,-gl_Position.y) + vec2(0.5);
}
