//post_process_bind_group
layout(binding = 0) uniform sampler texSampler;

layout(binding = 1) uniform texture2D scene_tex;
layout(binding = 2) uniform texture2D gui_tex;

layout(binding = 3) uniform texture2D index_tex_pf;
layout(binding = 4) uniform texture2D albedo_tex_pf;
layout(binding = 5) uniform texture2D normal_tex_pf;
layout(binding = 6) uniform texture2D position_tex_pf;
layout(binding = 7) uniform texture2D direct_light_tex_pf;
layout(binding = 8) uniform texture2D depth_tex_pf;
layout(binding = 9) uniform texture2D final_color_tex_pf;
layout(binding = 10) uniform texture2D indirect_light_tex_pf;
layout(binding = 11) uniform texture2D motion_tex_pf;

//helper_textures_bind_group
layout(set = 1, binding = 0, r32ui) uniform uimage2D index_texture;
layout(set = 1, binding = 1, rgba32f) uniform image2D g_albedo;
layout(set = 1, binding = 2, rgba8) uniform image2D g_normal;
layout(set = 1, binding = 3, rgba32f) uniform image2D g_position;
layout(set = 1, binding = 4, rgba32f) uniform image2D g_direct_light;
layout(set = 1, binding = 5, r32f) uniform image2D g_depth;
layout(set = 1, binding = 6, rgba16f) uniform image2D g_indirect_light;


layout(set = 1, binding = 7, r32ui) uniform uimage2D   store_index;
layout(set = 1, binding = 8, rgba32f) uniform image2D  store_albedo;
layout(set = 1, binding = 9, rgba8) uniform image2D    store_normal;
layout(set = 1, binding = 10, rgba32f) uniform image2D store_position;
layout(set = 1, binding = 11, rgba32f) uniform image2D store_direct_light;
layout(set = 1, binding = 12, r32f) uniform image2D    store_depth;
layout(set = 1, binding = 13, rgba32f) uniform image2D store_final_color;
layout(set = 1, binding = 14, rgba16f) uniform image2D store_indirect_light;
layout(set = 1, binding = 15, rgba16f) uniform image2D store_motion;

layout(set = 2, binding = 0) uniform cameraUniform {
    mat4 projection;
    mat4 proj;
    mat4 view;
    vec4 view_position;
};
layout(set = 2, binding = 1) buffer gg { float[16] param_cont; };
layout(set = 2, binding = 2) uniform rng { uint rng_seed; };
layout(set = 2, binding = 3) uniform pf_cameraUniform {
    mat4 pf_projection;
    mat4 pf_proj;
    mat4 pf_view;
    vec4 pf_view_position;
};