#version 460
#extension GL_GOOGLE_include_directive    : enable
#include "global_ubo.glsl"

#define NEAR 0.1
#define SIGMA 10.0
#define BSIGMA 0.1
#define MSIZE 15
const int kSize = (MSIZE-1)/2;

layout(location = 0) in vec2 texCoord_in;

// Converts a color from linear light gamma to sRGB gamma
vec3 fromLinear(vec3 linearRGB)
{
    bvec3 cutoff = lessThan(linearRGB, vec3(0.0031308));
    vec3 higher = vec3(1.055)*pow(linearRGB, vec3(1.0/2.4)) - vec3(0.055);
    vec3 lower = linearRGB * vec3(12.92);

    return mix(higher, lower, cutoff);
}

// Converts a color from sRGB gamma to linear light gamma
vec3 toLinear(vec3 sRGB)
{
    bvec3 cutoff = lessThan(sRGB, vec3(0.04045));
    vec3 higher = pow((sRGB + vec3(0.055))/vec3(1.055), vec3(2.4));
    vec3 lower = sRGB/vec3(12.92);

    return mix(higher, lower, cutoff);
}

layout(location = 0) out vec4 outColor;
void main() {
//    vec4 scene_color = texture(sampler2D(scene_tex, texSampler), texCoord_in);
    vec4 gui_color = texture(sampler2D(gui_tex, texSampler), texCoord_in);
    ivec2 resolution = imageSize(index_texture);

    vec2 norm_scr_coords = texCoord_in * 2.0 - 1.0;
    norm_scr_coords.y *= -1.0;

    //Data from path tracer
//    const uint index =          imageLoad(index_texture,    ivec2(gl_FragCoord.xy)).x;
//    const vec3 albedo =         imageLoad(g_albedo,         ivec2(gl_FragCoord.xy)).xyz;
//    const vec3 normal =         imageLoad(g_normal,         ivec2(gl_FragCoord.xy)).xyz;
//    const vec3 position =       imageLoad(g_position,       ivec2(gl_FragCoord.xy)).xyz;
//    const vec3 direct_light =   imageLoad(g_direct_light,   ivec2(gl_FragCoord.xy)).xyz;
//    const float depth =         imageLoad(g_depth,          ivec2(gl_FragCoord.xy)).x;
//    const vec3 indirect_light = imageLoad(g_indirect_light, ivec2(gl_FragCoord.xy)).xyz;

    const uint index =          imageLoad(store_index,          ivec2(gl_FragCoord.xy)).x;
    const vec3 albedo =         imageLoad(store_albedo,         ivec2(gl_FragCoord.xy)).xyz;
    const vec3 normal =         imageLoad(store_normal,         ivec2(gl_FragCoord.xy)).xyz;
    const vec3 position =       imageLoad(store_position,       ivec2(gl_FragCoord.xy)).xyz;
    const vec3 direct_light =   imageLoad(store_direct_light,   ivec2(gl_FragCoord.xy)).xyz;
    const float depth =         imageLoad(store_depth,          ivec2(gl_FragCoord.xy)).x;
    const vec3 indirect_light = imageLoad(store_indirect_light, ivec2(gl_FragCoord.xy)).xyz;

    //Previous frame data
//    const uint pf_index = texture(sampler2D(index_tex_pf, texSampler), texCoord_in).r;
//    const vec3 pf_albedo =          texture(sampler2D(albedo_tex_pf,            texSampler), texCoord_in).rgb;
//    const vec3 pf_normal =          texture(sampler2D(normal_tex_pf,            texSampler), texCoord_in).rgb;
//    const vec3 pf_position =        texture(sampler2D(position_tex_pf,          texSampler), texCoord_in).rgb;
//    const vec3 pf_direct_light =    texture(sampler2D(direct_light_tex_pf,      texSampler), texCoord_in).rgb;
//    const float pf_depth =           texture(sampler2D(depth_tex_pf,             texSampler), texCoord_in).r;
//    const vec4 pf_final_color =     texture(sampler2D(final_color_tex_pf,       texSampler), texCoord_in);
//    const vec3 pf_indirect_light =  texture(sampler2D(indirect_light_tex_pf,    texSampler), texCoord_in).rgb;
//    const vec3 pf_motion =          texture(sampler2D(motion_tex_pf,            texSampler), texCoord_in).rgb;


//    outColor.rgb = final_color * albedo;
//    outColor.rgb = imageLoad(store_direct_light, ivec2(gl_FragCoord.xy)).rgb + imageLoad(store_indirect_light, ivec2(gl_FragCoord.xy)).rgb;
    outColor.rgb = imageLoad(store_final_color, ivec2(gl_FragCoord.xy)).rgb;
//    outColor.rgb = vec3(float(pf_screen_pos.y > 0.9));

    //View debug layers (??? idk maybe useful)
    if (param_cont[4] != 0.0) {
        if        (param_cont[4] == 1.0){
            outColor = vec4(albedo, 1.0);
        } else if (param_cont[4] == 2.0){
            outColor = vec4(normal, 1.0);
        } else if (param_cont[4] == 3.0){
            outColor = vec4(position.xyz, 1.0);
        } else if (param_cont[4] == 4.0){
            outColor = vec4(direct_light, 1.0);
        } else if (param_cont[4] == 5.0){
            outColor = vec4(depth.xxx * 0.1, 1.0);
        } else if (param_cont[4] == 6.0){
            outColor = vec4((direct_light + indirect_light) * albedo, 1.0);
        } else if (param_cont[4] == 7.0){
            outColor = vec4(indirect_light, 1.0);
        }
    }

    outColor = vec4(mix(outColor.xyz, gui_color.xyz, gui_color.w), 1.0);

    //Save to use in pf_tex next frame
//    imageStore(store_index,          ivec2(gl_FragCoord.xy), index.xxxx);
//    imageStore(store_albedo,         ivec2(gl_FragCoord.xy), albedo.xyzz);
//    imageStore(store_normal,         ivec2(gl_FragCoord.xy), normal.xyzz);
//    imageStore(store_position,       ivec2(gl_FragCoord.xy), position.xyzz);
//    imageStore(store_direct_light,   ivec2(gl_FragCoord.xy), direct_light.xyzz);
//    imageStore(store_depth,          ivec2(gl_FragCoord.xy), depth.xxxx);
//    imageStore(store_final_color,    ivec2(gl_FragCoord.xy), vec4(final_color, ray_weight));
//    imageStore(store_indirect_light, ivec2(gl_FragCoord.xy), blurred_color.xyzz);
//    imageStore(store_motion,         ivec2(gl_FragCoord.xy), motion.xyzz);
}
