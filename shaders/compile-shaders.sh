glslc -fshader-stage=comp ./shader.frag --target-env=vulkan1.2 -o frag.spv
glslc -fshader-stage=frag ./post_process.frag --target-env=vulkan1.2 -o post_process_frag.spv
glslc -fshader-stage=comp ./filtering.comp --target-env=vulkan1.2 -o filtering.spv