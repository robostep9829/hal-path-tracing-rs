#version 460
#define VULKAN 100
#extension GL_EXT_ray_query : enable
#extension GL_EXT_scalar_block_layout: require
#extension GL_EXT_nonuniform_qualifier : require
#extension VK_KHR_relaxed_block_layout : enable

struct Vertex {
    vec3 positiom;
    vec2 texcoord;
    vec3 normal;
    uvec2 joints_packed;
    vec4 weights;
};
struct Mat {
    float roughness;
    float emission;
    vec3 normal;
    float depth;
};
layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;

layout(binding = 0) uniform texture2D inp_image[];
layout(binding = 1) uniform sampler texSampler[];
layout(binding = 2, scalar) buffer ind_buf {
    uint indbuf[];
} ind_cont[];
layout(binding = 3, scalar) buffer vtx_buf {
    Vertex vtxbuf[];
} vtx_cont[];

layout(set = 1, binding = 0) uniform cameraUniform {
    mat4 projection;
    mat4 proj;
    mat4 view;
    vec4 view_position;
};
layout(set = 1, binding = 1) buffer gg {
    float[16] param_cont;
};
layout(set = 1, binding = 2) uniform rng {
    uint rng_seed;
};

//Temporal solution for material lookup
Mat get_material(int ID){
    Mat res;
    switch (ID) {
        case 0: //cube
            res.roughness = 1;
            res.emission = 0.0;
            break;
        case 1: //sky sphere
            res.roughness = 1;
            res.emission = 1.0;
            break;
        case 2: //floor
            res.roughness = 0.0;
            res.emission = 0.0;
            break;
        case 3: //test object
            res.roughness = 1;
            res.emission = 0.0;
            break;
        case 4: //test object second material
            res.roughness = 1;
            res.emission = 0.0;
            break;
        case 5: //emissive sphere
            res.roughness = 1;
            res.emission = 20.0;
            break;
        case 6: //skeletal mesh
            res.roughness = 1;
            res.emission = 0.0;
            break;
    }
    return res;
}

layout(set = 2, binding = 0, rgba8) uniform image2D img_data; //Actual render output
layout(set = 2, binding = 1) uniform accelerationStructureEXT topLevelAS;

layout(set = 3, binding = 0, r32ui) uniform uimage2D index_texture;
layout(set = 3, binding = 1, rgba32f) uniform writeonly image2D g_albedo;
layout(set = 3, binding = 2, rgba8) uniform writeonly image2D g_normal;
layout(set = 3, binding = 3, rgba32f) uniform writeonly image2D g_position;
layout(set = 3, binding = 4, rgba32f) uniform writeonly image2D g_direct_light;
layout(set = 3, binding = 5, r32f) uniform writeonly image2D g_depth;
layout(set = 3, binding = 6, rgba16f) uniform writeonly image2D g_indirect_light;

const vec3 colors[3] = vec3[](vec3(1.0, 0.3, 0.3), vec3(0.3, 1.0, 0.3), vec3(0.3, 0.3, 1.0));
const float k_pi = 3.14159265;
const int NUM_SAMPLES = 4;

float stepAndOutputRNGFloat(inout uint rngState)
{
    // Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
    rngState = rngState * 747796405 + 1;
    uint word = ((rngState >> ((rngState >> 28) + 4)) ^ rngState) * 277803737;
    word = (word >> 22) ^ word;
    return float(word) / 4294967295.0f;
}
vec2 hash2(inout uint rngState){
    return vec2(stepAndOutputRNGFloat(rngState), stepAndOutputRNGFloat(rngState));
}
bool close_to_zero(vec3 vec){
    const float eps = 0.0001;
    return (abs(vec.r) < eps && abs(vec.g) < eps && abs(vec.b) < eps);
}
vec2 uniformSampleDisk(const vec2 u){
    float r = sqrt(u.r);
    float theta = 2 * k_pi * u.g;
    return vec2(r * cos(theta), r * sin(theta));
}
vec2 concentricSampleDisk(const vec2 u){
    vec2 uOffset = 2.0 * u - vec2(1, 1);
    if (uOffset.x == 0.0 && uOffset.y == 0){
        return vec2(0);
    }
    float theta, r;
    if (abs(uOffset.x) > abs(uOffset.y)){
        r = uOffset.x;
        theta = (k_pi / 4) * (uOffset.y / uOffset.x);
    } else {
        r = uOffset.y;
        theta = (k_pi / 2) - (k_pi / 4) * (uOffset.x / uOffset.y);
    }
    return r * vec2(cos(theta), sin(theta));
}
vec3 cosineSampleHemisphere(const vec2 u){
    vec2 d = concentricSampleDisk(u);
    float z = sqrt(max(0.0, 1 - d.x * d.x - d.y * d.y));
    return vec3(d.x, d.y, z);
}
float cosineHemispherePdf(float cosTheta){
    return cosTheta / k_pi;
}
vec3 cosWeightedRandomHemisphereDirection( const vec3 n, inout uint seed ) {
    vec2 r = vec2(stepAndOutputRNGFloat(seed), stepAndOutputRNGFloat(seed));

    vec3  uu = normalize( cross( n, vec3(0.0,1.0,1.0) ) );
    vec3  vv = cross( uu, n );

    float ra = sqrt(r.y);
    float rx = ra*cos(6.2831*r.x);
    float ry = ra*sin(6.2831*r.x);
    float rz = sqrt( 1.0-r.y );
    vec3  rr = vec3( rx*uu + ry*vv + rz*n );

    return normalize( rr );
}
const mat3 RGBToYCoCgMatrix = mat3(0.25, 0.5, -0.25, 0.5, 0.0, 0.5, 0.25, -0.5, -0.25);

const mat3 YCoCgToRGBMatrix = mat3(1.0, 1.0, 1.0, 1.0, 0.0, -1.0, -1.0, 1.0, -1.0);

//https://users.cg.tuwien.ac.at/zsolnai/gfx/smallpaint/
// given v1, set v2 and v3 so they form an orthonormal system
// (we assume v1 is already normalized)
void ons(const vec3 v1, inout vec3 v2, inout vec3 v3) {
    if (abs(v1.x) > abs(v1.y)) {
        // project to the y = 0 plane and construct a normalized orthogonal vector in this plane
        float invLen = 1.f / sqrt(v1.x * v1.x + v1.z * v1.z);
        v2 = vec3(-v1.z * invLen, 0.0f, v1.x * invLen);
    } else {
        // project to the x = 0 plane and construct a normalized orthogonal vector in this plane
        float invLen = 1.0f / sqrt(v1.y * v1.y + v1.z * v1.z);
        v2 = vec3(0.0f, v1.z * invLen, -v1.y * invLen);
    }
    v3 = cross(v1, v2);
}
vec3 sampleDiffuse(vec3 world_Norm, uint rngState){
    vec3 rotX, rotY;
    ons(world_Norm, rotX, rotY);
    //vec3 sampledDir = cosineWeightedHemisphere(stepAndOutputRNGFloat(rngState), stepAndOutputRNGFloat(rngState));
    vec3 sampledDir = cosineSampleHemisphere(hash2(rngState));
    vec3 rotatedDir;
    rotatedDir.x = dot(vec3(rotX.x, rotY.x, world_Norm.x),sampledDir);
    rotatedDir.y = dot(vec3(rotX.y, rotY.y, world_Norm.y),sampledDir);
    rotatedDir.z = dot(vec3(rotX.z, rotY.z, world_Norm.z),sampledDir);
    return rotatedDir;
}
vec3 randomSphereDirection(inout uint seed) {
    vec2 h = hash2(seed) * vec2(2.,6.28318530718)-vec2(1,0);
    float phi = h.y;
    return vec3(sqrt(1.-h.x*h.x)*vec2(sin(phi),cos(phi)),h.x);
}

void main() {
    const ivec2 resolution = imageSize(img_data);
    const uvec2 pixel = gl_GlobalInvocationID.xy;
    // If the pixel is outside of the image, don't do anything:
    if ((pixel.x >= resolution.x) || (pixel.y >= resolution.y))
    {
        return;
    }
    const vec2 pixelCenter = pixel + vec2(0.5);
    const vec2 inUV = pixelCenter / vec2(resolution);
    vec2 d = inUV * 2.0 - 1.0;
    d.y *= -1;
    uint rngState = resolution.x * pixel.y + pixel.x;
    rngState += rng_seed;
    float tMin = 0.001;
    float tMax = 10000;

    float param = param_cont[0];
    vec2 mouse_pos = vec2(param_cont[1], param_cont[2]);
    float out_index = param_cont[3];

    vec3 direct_light = vec3(0.00001);
    vec3 indirect_light = vec3(0.00001);
    vec3 albedo = vec3(0.0);
    float depth = 0.0;
    vec3 g_normal_value = vec3(0.0);
    vec3 g_position_value = vec3(0.0);

    vec3 color_sum = vec3(0);
    for (int i_sample = 0; i_sample < NUM_SAMPLES; i_sample++)
    {
        vec4 origin = view_position;
        vec4 direction = inverse(view) * vec4(((inverse(proj) * vec4(d.x, d.y, 1, 1)).xyz), 0.0);
        direction.xyz = normalize(direction.xyz);
        vec3 add_color = vec3(0.0);
        vec3 throughtput = vec3(1.0);
        vec3 world_Pos;
        vec3 world_Norm;
        uint g_index = 0;
        Mat last_material;
        for (int i = 0; i < 6; i++) {
            rayQueryEXT rq;
            rayQueryInitializeEXT(rq, topLevelAS, 0, 0xff, origin.rgb, tMin, direction.xyz, tMax);
            while (rayQueryProceedEXT(rq)) {};
            if (rayQueryGetIntersectionTypeEXT(rq, true) == gl_RayQueryCommittedIntersectionNoneEXT)
            {return;}
            const int ray_ind = rayQueryGetIntersectionInstanceCustomIndexEXT(rq, true);

            const int geom_ind = rayQueryGetIntersectionGeometryIndexEXT(rq, true);
            const int primitiveID = rayQueryGetIntersectionPrimitiveIndexEXT(rq, true);
            const mat4x3 obj_to_world = rayQueryGetIntersectionObjectToWorldEXT(rq, true);

            const int buf_id = ray_ind + geom_ind;

            const uint i0 = ind_cont[nonuniformEXT(buf_id)].indbuf[3 * primitiveID + 0];
            const uint i1 = ind_cont[nonuniformEXT(buf_id)].indbuf[3 * primitiveID + 1];
            const uint i2 = ind_cont[nonuniformEXT(buf_id)].indbuf[3 * primitiveID + 2];

            const Vertex v0 = vtx_cont[nonuniformEXT(buf_id)].vtxbuf[i0];
            const Vertex v1 = vtx_cont[nonuniformEXT(buf_id)].vtxbuf[i1];
            const Vertex v2 = vtx_cont[nonuniformEXT(buf_id)].vtxbuf[i2];

            vec3 barycentrics = vec3(0.0, rayQueryGetIntersectionBarycentricsEXT(rq, true));
            barycentrics.x = 1.0 - barycentrics.y - barycentrics.z;

            const vec3 objectPos = v0.positiom * barycentrics.x + v1.positiom * barycentrics.y + v2.positiom * barycentrics.z;
            const vec3 objectNormal = v0.normal * barycentrics.x + v1.normal * barycentrics.y + v2.normal * barycentrics.z;
            const vec2 objectUV = v0.texcoord * barycentrics.x + v1.texcoord * barycentrics.y + v2.texcoord * barycentrics.z;
            world_Pos = obj_to_world * vec4(objectPos, 1.0);
            world_Norm = obj_to_world * vec4(objectNormal, 0.0);
            float rayT = rayQueryGetIntersectionTEXT(rq, true);
            Mat mat = get_material(ray_ind);
            mat.normal = world_Norm;
            mat.depth = rayT;
            vec3 albedo_hit = texture(sampler2D(inp_image[nonuniformEXT(buf_id)], texSampler[nonuniformEXT(buf_id)]), objectUV).xyz;

            if (i == 0 /* Visibility ray */){
                g_normal_value = world_Norm;
                g_position_value = world_Pos;
                depth = rayT;
                albedo = albedo_hit;
                g_index = rayQueryGetIntersectionInstanceIdEXT(rq, true);
                imageStore(index_texture, ivec2(pixel), uvec4(g_index, 0, 0, 0));
                last_material = mat;
            } else if (i == 1 && last_material.roughness <= 0.2) { //First bounce normals
                g_normal_value = reflect(world_Norm, last_material.normal);
                depth = rayT + last_material.depth;
            }
            origin.xyz = world_Pos;
            direction.xyz = mix(reflect(direction.xyz, world_Norm), cosWeightedRandomHemisphereDirection(world_Norm, rngState), mat.roughness);
            if (close_to_zero(direction.xyz)){ direction.xyz = world_Norm; }
            direction.xyz = normalize(direction.xyz);
            origin.xyz += world_Norm * 0.001;

            if (mat.emission != 0.0){
                add_color += throughtput * mat.emission * albedo_hit;
                if (i <= 1) {direct_light += add_color;} // First reflected ray hit light source (direct light) or camera sees light
                break;
            }

            float cosTheta = dot(direction.xyz, world_Norm);
            float pdf = cosineHemispherePdf(cosTheta);
            throughtput *= (albedo_hit * cosTheta / k_pi) / pdf;
        }
        color_sum += add_color;
    }

    direct_light /= NUM_SAMPLES;
    vec3 final_color = color_sum / float(NUM_SAMPLES);
    indirect_light = final_color - direct_light;

    imageStore(img_data, ivec2(pixel), vec4(final_color, 1));
//    imageStore(g_final_color, ivec2(pixel), vec4(final_color, 1));
    albedo.x = max(0.0001, albedo.x);
    albedo.y = max(0.0001, albedo.y);
    albedo.z = max(0.0001, albedo.z);

    imageStore(g_albedo, ivec2(pixel), vec4(albedo, 1.0));
    imageStore(g_normal, ivec2(pixel), vec4(g_normal_value, 1.0));
    imageStore(g_position, ivec2(pixel), vec4(g_position_value, 1.0));
    imageStore(g_direct_light, ivec2(pixel), vec4(direct_light / albedo, 1.0));
    imageStore(g_depth, ivec2(pixel), vec4(depth));
    imageStore(g_indirect_light, ivec2(pixel), vec4(indirect_light / albedo, 1.0));

    out_index = imageLoad(index_texture, ivec2(mouse_pos)).x;

    param_cont = float[](param, mouse_pos[0], mouse_pos[1], out_index, param_cont[4] /* Debug layer */, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
}
